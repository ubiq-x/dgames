# DGames
A Web application for administering experiments consisting of sequences of decision games.


## Abstract
Game theory is a branch of mathematics that studies interactions between two or more agents who can be either humans or computer models. Those interactions are often studied in the context of games that present the agents with a sequence of decisions. Each decision is associated with a reward or punishment and the research focus is on conflict and cooperation between the agents.  This repository presents a Web application designed to administer experiments consisting of sequences of such decisions.


## Contents
- [Features](#features)
- [Details](#details)
  * [Games and Experiments](#games-and-experiments)
  * [Subjects](#subjects)
  * [Experiment UI](#experiment-ui)
  * [Experiment Manager Console](#experiment-manager-console)
  * [Experiment Simulator Console](#experiment-simulator-console)
  * [Reports](#reports)
  * [Amazon Mechanical Turk Integration](#amazon-mechanical-turk-integration)
- [Project Status](#project-status)
- [Dependencies](#dependencies)
- [Data Structures](#data-structures)
- [Setup](#setup)
  * [Database Server and The Database](#database-server-and-the-database)
  * [Web Container and Logic](#web-container-and-logic)
- [Acknowledgments](#acknowledgments)
- [References](#references)
- [Citing](#citing)
- [License](#license)


## Features
- Present experimental subjects with a sequence of decision games, input prompts, and HTML pages with JavaScript interactivity
- Assign subjects to a randomly selected condition in a counter-balanced manner
- Synthetic subjects console for easier testing before and after deployment
- Integration with Amazon Mechanical Turk


## Details
### Games and Experiments
An example of a decision game used by game theory researchers to study human decision making is a _probability game_ where the player makes a choice between the two following alternatives:
- Receive $100 with probability of 0.1 and $0 with probability 0.9
- Receive $10 with certainty
Of course, this kind of a game is only played by one agent.

DGames allows the administration of games just like this one and doing so in a very flexible manner.  For example, the researcher is free to define the parameters of any game as well as the sequence of what the subject sees in each of the experimental conditions they might be assigned to.  Apart from decision games, the subject can be presented with two other types of items.  First, an HTML page (with JavaScript) can be displayed which is useful for instructions, debriefings, etc.  Second, an input prompt can be displayed in order to collect a response from subjects (e.g., as a part of a questionnaire).  There can be an unlimited amount of games, HTML pages, and prompts shown in any condition.  To foster generality, in this software decision games are called _models_ and input prompts are called _variables_.

Each individual model can implement one of the following three paradigms: (a) Decision from description, (b) decision from experience, or (c) consequential choice.  The current implementation of this software does not support other types of games.  In that respect, this software is a prototype because the experiment administration component is mature and complete but other types of games need to be implemented before new experiments can be run.  One example of a class of games like that are _NxN games_ where the player plays against other human players or against computational models of decision making such as the Instance-Based Learning (IBL; Gonzalez, 2003) model.  An example of a NxN game is the _Prisoner's Dilemma_.  In this 2x2 game, each of the two players is a captured prisoner.  The prisoners are questioned in separate rooms and each player has two options: To give they friend away (i.e., to _defect_) or not to give them away (i.e., to _cooperate_).  Depending on the decision of both players (not just one), they get different payoffs (i.e., the payoff is modeled jointly).

The user interface of an experiment is a page-replacement free Web application the state of which is controlled by the server.  Thanks to this design there is no risk that an accidental page reload would disturb the subject's session.  If a reload happens, upon loading again, the experiment page requests the state it should be in and resets itself accordingly allowing the subject to continue from exactly the same point.  This behavior is especially important because most of the subjects are expected to participate on-line only.

### Subjects
The primary source of experimental subjects is the Amazon Mechanical Turk (AMT) workforce pool, but the software is able to accommodate regular subjects as well.  A new subject is automatically assigned to a randomly selected condition in a counter-balanced manner.  Different assignment and randomization schemes can be implemented as well.

Synthetic subjects can be spawned as needed.  Those subjects do not interact with any ongoing experiments which lends itself to creating a seamless testing environment.

### Experiment UI
The experiment UI is what the subjects interact with and is the component responsible for all data collection.  It has been designed to be embedded into the AMT Web site.  Nevertheless, it can also be used stand-alone, ideally in a pop-up window in order to prevent the subject from tampering with query string arguments.

![Experiment: A list question](media/exp-01.png)
<p align="center"><b>Figure 1. </b>An experiment: A list question.</p>

![Experiment: A required response](media/exp-02.png)
<p align="center"><b>Figure 2. </b>An experiment: A required response.</p>

Figures 1-4 show some of the capabilities if the experiment UI.  Specifically, Figures 1 and 2 show a page collecting a one-out-of-many response as well as the way the subject is informed about their answer being compulsory (responses to not all questions are required).

![Experiment: Probability game](media/exp-03.png)
<p align="center"><b>Figure 3. </b>An experiment: First probability game.</p>

![Experiment: Probability game](media/exp-04.png)
<p align="center"><b>Figure 4. </b>An experiment: Second probability game.</p>

Figures 3 and 4 show two different probability games supported by this software.  In the first one, the subject makes a choice between two alternatives shown.  The "continue" button is shown only after a choice has been made.  In the second one, the subject explores the two alternatives only to be asked about their choice between them on the next page.  This two-page design was significant because it allowed to address a specific research question.

### Experiment Manager Console
Figure 5 shows the interface used to manage an existing experiment.  Apart from having an overview of the experiment, the researcher can fine tune the parameters shown in bold (e.g., the base AMT pay or the maximum number of subjects in each condition).

![Experiment Manager](media/exp-man.png)
<p align="center"><b>Figure 5. </b>Experiment Manager.</p>

### Experiment Simulator Console
Despite being called "simulator," this is the central interface of the present software.  It has been designed primarily with testing in mind and as such offers full control over synthetic subjects.  As shown on Figure 6, the interface allows the researcher to select the experiment, start a synthetic subject session as well as end all ongoing synthetic subject sessions.  All synthetic subjects go through regular condition assignment and do not compete for spots with real subjects.  That is, synthetic subjects can be used side by side real subjects with no consequence for on-going experiments.

![Experiment Simulator: ](media/exp-sim.png)
<p align="center"><b>Figure 6. </b>Experiment Simulator.  As indicated by the left panel, no new subject sessions can be created because the experiment is full.</p>

The Experiment Manager console described in the previous subsection can be started from here as well.  Moreover, all synthetic subjects can be removed from here as well.  This interface does not interfere with real subjects or their sessions.

### Reports
The Experiment Simulator interface also provides access to the following reports:

- Synthetic (or simulated) subject sessions ended
- Synthetic (or simulated) subject sessions live
- Real subject sessions ended (Figures 7 and 8)
- Real subject sessions live
- Final (used by the researcher in statistical analyses)
- AMT (this will work only if the software is connected to the AMT which requires a pair of AMT-generated keys)

Each of those reports reflects the current state of the database and therefore can be updated by simply refreshing the page.

![Experiment report (part 1)](media/rep-01.png)
<p align="center"><b>Figure 7. </b>Report of real subject sessions which have ended.  Part 1: Experiment, subjects, and sessions.</p>

![Experiment report (part 2)](media/rep-02.png)
<p align="center"><b>Figure 8. </b>Report of real subject sessions which have ended.  Part 2: Trials and variables.</p>

Note that Figures 7 and 8 have missing values for all AMT-related variables.  That is because those screenshots have been taken with the software not connected to the AMT; appropriate values would be displayed otherwise.

### Amazon Mechanical Turk Integration
In the initial design, I attempted to make a very tight integration between DGames and the AMT.  For example, the software was able to publish tasks directly to the AMT, automatically approve tasks completed by AMT workers and grant them bonuses proportional to their results.  To my dismay, however, the AMT Java library consisted of a staggering number of libraries.  I always err on the side of simplicity and only use complex software if absolutely necessary.  Because of that, I chose to implement my own communication layer between the present software and the AMT.  Unfortunately, due to immaturity of the AMT framework (specifically, massive documentation gaps and huge inadequacies of their testing environment called the <i>sandbox</i>) I have ultimately abandoned the idea of this software communicating with the AMT during an experiment.  Instead, in its current incarnation, the software is invoked from the AMT and managed manually thereafter.  For example, subject tasks need to be approved manually.  This does not mean, however, that there is no integration; rather that I strived for a completely management-free implementation which could not be realized given the short timeframe of the project.  Indeed, the present software appropriately records values of all AMT-related variables (e.g., the worker ID).

Finally, I want to emphasize that I wrote this software in 2012 and it is possible the AMT has matured since then.  That is why I include the AMT-related Java code in this repository (under [`logic/src/amazon`](logic/src/amazon)).  Note that _access&#95;key_ and _secret&#95;key_ are required to enable DGames to interact with the AMT system.


## Project Status
I use this repository to publish source code associated with my original work done in 2012.  While further work might have been carried out as an extension to what I publish here, I see this code as a milestone that marks the end of my involvement in the project and hope it will be useful to someone in its own right.


## Dependencies
- [PostgreSQL](https://www.postgresql.org)
- [PostgreSQL JDBC driver](https://jdbc.postgresql.org)
- [Java Runtime Environment](https://java.com)
- [Apache HttpComponents](https://hc.apache.org)
- [Apache Ant](https://ant.apache.org) or an IDE (e.g., [Eclipse](https://www.eclipse.org/downloads/))
- A web container (e.g., [Apache Tomcat](http://tomcat.apache.org))


## Data Structures
Main data structures are defined by the [`db/ddl.sql`](db/ddl.sql) script and are robust (through constraints), efficient (through normalization and indexing), and extensible (through proper decomposition).  The `experiment` is the center object; each experiment is associated with a list of `subjects` (be it AMT, physical, or synthetic), `models` (e.g., probability games), `HTML pages` (i.e., arbitrary information with interactivity), `variables` (i.e., input prompts), and experimental `conditions`.  Each experimental condition consist of a sequence of `blocks` and a block is an invocation of a model, HTML page, or a variable.  This design allows for a model, an HTML page, or a variable to be defined once and then invoked in different conditions and in a completely arbitrary order.  A variable can be of type text, single-choice, and multiple-choice.  The text shown on HTML pages can be parameterized with placeholders.  For example, the current AMT bonus that the subject will receive; the value of that bonus can be conditioned upon the history of the subject's in-game choices.


## Setup
Begin by cloning this repository:
```
git clone https://gitlab.com/ubiq-x/dgames
cd dgames
```

### Database Server and The Database
Install [PostgreSQL](https://www.postgresql.org) and configure it to your liking.  Then, execute the [`db/ddl.sql`](db/ddl.sql) DDL SQL script against the server to create the `dgames` database:
```
psql -U postgres -W < db/ddl.sql
```
To populate the database with a test experiment, execute the [`db/dml-test.sql`](db/dml-test.sql) script as well:
```
psql -U postgres -W -d dgames < db/ddm-test.sql
```
There currently is no way of adding experiments other than through SQL scripts so use the test script as a template.

### Web Container and Logic
- Install [Java Runtime Environment](https://java.com).
- Install a web container of your choice (e.g., [Apache Tomcat](http://tomcat.apache.org)) and configure it to your liking.
- Download the latest [PostgreSQL JDBC driver](https://jdbc.postgresql.org) and put in the appropriate place (e.g., `<catalina-home>/lib/` for Tomcat).
- Create the serving directory for the project (referred to here as `<project-root>`) and copy into it the contents of the `ui` directory.
- If you're not using an IDE such as Eclipse, install [Apache Ant](https://ant.apache.org) and download [Apache HttpComponents](https://hc.apache.org) (although v4.5.5 is included in the [logic/lib/](logic/lib/) directory).
- Compile the Java sources from the `logic-src` directory and copy the byte code into the appropriate place (e.g., `<project-root>/WEB-INF/classes/` on Tomcat; see [`build.xml`](build.xml) Ant buildfile).

If you use Tomcat, the bits you should execute boil down to several simple steps (after installing all dependencies that is).  First, edit [`build.xml`](build.xml) buildfile and tailor `lib.tomcat` and `webapps` properties to your system.  Then, edit the [`logic/META-INF/context.xml`](logic/META-INF/context.xml) and enter your database credentials.  Finally, execute the following in the terminal after having set the  Tomcat path appropriate to your system (i.e., edit the first line):
```sh
dir=/usr/local/apache-tomcat-7.0.53/webapps/dgames  # tailor to your system

mkdir -p $dir/WEB-INF/classes
mkdir -p $dir/WEB-INF/lib

cp -R ./ui/* $dir
cp -R ./ui/logic/META-INF $dir
cp -R ./logic/lib/httpcomponents-client-4.5.5/lib/* $dir/WEB-INF/lib

ant deploy
```
For the default Tomcat port, the Web app will be available at `http://localhost:8080/dgames` and you will be able to access the following pages:

- [Experiment Simulator](http://localhost:8080/dgames/exp-sim.html) (described [above](#experiment-simulator-console))
- [Experiment Manager](http://localhost:8080/dgames/exp-man.html?exp-id=2) (described [above](#experiment-manager-console))
- [Experiment](http://localhost:8080/dgames/exp.html?exp-id=2&is-sim=1) (described [above](#experiment-ui))


## Acknowledgments
I worked on this project as a member of the [Dynamic Decision Making Lab](http://www.hss.cmu.edu/departments/sds/ddmlab) at Carnegie Melon University.


## References
Gonzalez, C., Lerch, J.F., & Lebiere, C. (2003) Instance-Based Learning in Dynamic Decision Making.  _Cognitive Science, 27(4)_, 591-635.


## Citing
Loboda, T.D. (2012).  DGames [computer software].  Available at _https://gitlab.com/ubiq-x/dgames_


## License
This project is licensed under the [BSD License](LICENSE.md).
