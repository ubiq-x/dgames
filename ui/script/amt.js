var CONST = {
  msgErr    : "Operation unsuccessfull. This event has been logged and can be investigated later.",
  urlServer : "/dgames/do?"
};


// ----------------------------------------------------------------------------------------------------------
function amtApproveAssign(sessId, assignId, isProd) {
  $call("GET", CONST.urlServer + "cmd=amt-approve-assign&assign-id=" + assignId + "&is-prod=" + (isProd ? 1 : 0), null, function (res) { amtApproveAssign_cb(sessId, res); }, true, false);
}


// - - - -
function amtApproveAssign_cb(sessId, res) {
  //if (!res.outcome) return alert(CONST.msgErr);
  
  $("td-approve-" + sessId).innerHTML = "Approved";
}


// ----------------------------------------------------------------------------------------------------------
function amtGrantBonus(sessId, assignId, bonus, bonusMax, isProd) {
  if (bonus === null) bonus = Math.round($("txt-bonus-" + sessId).value * 100);
  if (isNaN(bonus)) return alert("Invalid amount specified.");
  if (bonus <= 0) return alert("The amount has to be positive.");
  if (bonus > bonusMax && prompt("You are about to grant bonus exceeding the maximum specified by the researcher. Type OK (case sensitive) below to continue.") != "OK") return;
  
  $call("GET", CONST.urlServer + "cmd=amt-grant-bonus&assign-id=" + assignId + "&bonus=" + bonus + "&is-prod=" + (isProd ? 1 : 0), null, function (res) { amtGrantBonus_cb(sessId, res); }, true, false);
}


// - - - -
function amtGrantBonus_cb(sessId, res) {
  //if (!res.outcome) return alert(CONST.msgErr);
  
  $("td-bonus-" + sessId).innerHTML = "Granted";
}
