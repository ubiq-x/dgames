DROP DATABASE IF EXISTS "dgames";
CREATE DATABASE "dgames" WITH ENCODING 'UTF8' TEMPLATE=template0;

\c dgames

BEGIN;


----------------------------------------------------------------------------------------------------
DROP TABLE IF EXISTS model_ibl;
DROP TABLE IF EXISTS model_prob;
DROP TABLE IF EXISTS sub;
DROP TABLE IF EXISTS exp;
DROP TABLE IF EXISTS cond;
DROP TABLE IF EXISTS block;
DROP TABLE IF EXISTS sess;
DROP TABLE IF EXISTS trial;


----------------------------------------------------------------------------------------------------
-- Reset sequences (TODO)


----------------------------------------------------------------------------------------------------
CREATE TABLE exp (
  id BIGSERIAL PRIMARY KEY NOT NULL,
  created timestamp NOT NULL DEFAULT now(),
  name varchar(128) NOT NULL UNIQUE,
  do_allow_mult_sub boolean NOT NULL DEFAULT false,
  amt_assign_cnt smallint NOT NULL DEFAULT 1 CHECK (amt_assign_cnt >= 1),
  amt_pay_base smallint NOT NULL DEFAULT 0 CHECK (amt_pay_base >= 0),
  amt_t_exp integer NOT NULL DEFAULT 7776000 CHECK (amt_t_exp >= 1),
  amt_t_sess integer NOT NULL DEFAULT 600 CHECK (amt_t_sess >= 30),
  amt_frame_h smallint,
  note text
);

COMMENT ON COLUMN exp.amt_assign_cnt IS 'Amazon Mechanical Turk - The number of worker sessions (assignments) that can be happening simulatously (not actually usable within the AMT API itself)';
COMMENT ON COLUMN exp.amt_pay_base   IS 'Amazon Mechanical Turk - Base pay a subject will get by completing this condition [cents]';
COMMENT ON COLUMN exp.amt_t_exp      IS 'Amazon Mechanical Turk - The amount of time tasks will be available for workers to complete [sec] (default is 90 days)';
COMMENT ON COLUMN exp.amt_t_sess     IS 'Amazon Mechanical Turk - Time limit to complete a single task [sec]';
COMMENT ON COLUMN exp.amt_frame_h    IS 'Amazon Mechanical Turk - Frame height [px]';


----------------------------------------------------------------------------------------------------
CREATE TABLE sub (
  id BIGSERIAL PRIMARY KEY NOT NULL,
  created timestamp NOT NULL DEFAULT now(),
  exp_id bigint NOT NULL REFERENCES exp(id) ON DELETE CASCADE ON UPDATE CASCADE,
  uname varchar(16) UNIQUE,
  is_sim boolean NOT NULL DEFAULT true,
  amt_worker_id varchar(128) UNIQUE,
  amt_pay_base smallint CHECK (amt_pay_base >= 0),
  amt_pay_bonus smallint CHECK (amt_pay_bonus >= 0),
  amt_approve_ts timestamp,
  amt_reject_ts timestamp,
  amt_bonus_ts timestamp,
  note text
);

COMMENT ON COLUMN sub.is_sim         IS 'Is the subject a simulated one (removal of which is inconsequential)?';
COMMENT ON COLUMN sub.amt_worker_id  IS 'Amazon Mechanical Turk - Worker ID';
COMMENT ON COLUMN sub.amt_pay_base   IS 'Amazon Mechanical Turk - Base pay the subject has received (NULL indicated no pay) [cents]';
COMMENT ON COLUMN sub.amt_pay_bonus  IS 'Amazon Mechanical Turk - Bonus pay the subject has received (NULL indicated no pay) [cents]';

DROP INDEX IF EXISTS sub_exp_id;  CREATE INDEX sub_exp_id ON exp (id);


----------------------------------------------------------------------------------------------------
CREATE TABLE model_ibl (
  id BIGSERIAL PRIMARY KEY NOT NULL,
  created timestamp NOT NULL DEFAULT now(),
  exp_id bigint NOT NULL REFERENCES exp(id) ON DELETE CASCADE ON UPDATE CASCADE,
  name varchar(64) NOT NULL,
  noise real CHECK (noise >= 0 AND noise <= 1),
  decay real CHECK (decay >= 0 AND decay <= 1),
  note text,
  UNIQUE(exp_id, name)
);

COMMENT ON COLUMN model_ibl.noise IS 'Model parameter: Noise in the activation equation (sigma)';
COMMENT ON COLUMN model_ibl.decay IS 'Model parameter: Decay in the activation equation (d)';

DROP INDEX IF EXISTS model_ibl_exp_id;  CREATE INDEX model_ibl_exp_id ON exp (id);


----------------------------------------------------------------------------------------------------
CREATE TABLE model_prob (
  id BIGSERIAL PRIMARY KEY NOT NULL,
  created timestamp NOT NULL DEFAULT now(),
  exp_id bigint NOT NULL REFERENCES exp(id) ON DELETE CASCADE ON UPDATE CASCADE,
  name varchar(64) NOT NULL,
  p_a_0 real CHECK (p_a_0 >= 0 AND p_a_0 <= 1),
  p_b_0 real CHECK (p_b_0 >= 0 AND p_b_0 <= 1),
  payoff_a_0 real,
  payoff_a_1 real,
  payoff_b_0 real,
  payoff_b_1 real,
  txt_top text,
  txt_btn_a varchar(255),
  txt_btn_b varchar(255),
  txt_pre text,
  txt_post text,
  txt_payoff_pre varchar(16),
  txt_payoff_post varchar(16),
  lbl_btn_prev varchar(255),
  lbl_btn_next varchar(255),
  note text,
  UNIQUE(exp_id, name)
);

COMMENT ON COLUMN model_prob.txt_top         IS 'Text on top of the page';
COMMENT ON COLUMN model_prob.txt_pre         IS 'Text before the game has started';
COMMENT ON COLUMN model_prob.txt_post        IS 'Text after the game has ended';
COMMENT ON COLUMN model_prob.txt_payoff_pre  IS 'Text displayed before all payoffs (e.g., $)';
COMMENT ON COLUMN model_prob.txt_payoff_post IS 'Text displayed after all payoffs (e.g., EUR)';

DROP INDEX IF EXISTS model_prob_exp_id;  CREATE INDEX model_prob_exp_id ON exp (id);


----------------------------------------------------------------------------------------------------
CREATE TABLE html (
  id BIGSERIAL PRIMARY KEY NOT NULL,
  created timestamp NOT NULL DEFAULT now(),
  exp_id bigint NOT NULL REFERENCES exp(id) ON DELETE CASCADE ON UPDATE CASCADE,
  name varchar(64) NOT NULL,
  html text,
  url text,
  js_fn_prev text,
  js_fn_next text,
  lbl_btn_prev varchar(255),
  lbl_btn_next varchar(255),
  min_read_time smallint NOT NULL DEFAULT 0 CHECK (min_read_time >= 0),
  note text,
  UNIQUE(exp_id, name)
);

COMMENT ON COLUMN html.min_read_time   IS 'The minimum time it takes to read the page (0=no limit) [ms]';

DROP INDEX IF EXISTS html_exp_id;  CREATE INDEX html_exp_id ON exp (id);


----------------------------------------------------------------------------------------------------
-- CREATE TYPE var_type    AS ENUM ('bin', 'cat', 'ord', 'dis', 'con', 'str');

CREATE TYPE var_type AS ENUM ('txt-line', 'txt-block', 'options-one', 'options-multiple');

CREATE DOMAIN var_name_domain AS varchar(128) CHECK(VALUE ~ E'[\w-]*');

CREATE TABLE var (
  id BIGSERIAL PRIMARY KEY NOT NULL,
  created timestamp NOT NULL DEFAULT now(),
  exp_id bigint NOT NULL REFERENCES exp(id) ON DELETE CASCADE ON UPDATE CASCADE,
  name var_name_domain NOT NULL,
  type var_type NOT NULL,
  txt_lbl text NOT NULL,
  txt_help text,
  is_req boolean NOT NULL DEFAULT true,
  --max_resp_time smallint NOT NULL DEFAULT 0 CHECK (max_rerp_time >= 0),
  note text,
  UNIQUE(exp_id, name)
);

COMMENT ON COLUMN var.name          IS 'Name is outputted in all relevant reports.';
--COMMENT ON COLUMN var.max_resp_time IS 'The maximum time give for the response (0=no limit) [sec].';

DROP INDEX IF EXISTS var_exp_id;  CREATE INDEX var_exp_id ON exp (id);


----------------------------------------------------------------------------------------------------
CREATE TABLE var_val (
  id BIGSERIAL PRIMARY KEY NOT NULL,
  var_id bigint NOT NULL REFERENCES var(id) ON DELETE CASCADE ON UPDATE CASCADE,
  val varchar(64),
  idx smallint CHECK (idx >= 0),
  txt_lbl varchar(128) NOT NULL,
  note text,
  UNIQUE(var_id, idx)
);

DROP INDEX IF EXISTS var_val_var_id;  CREATE INDEX var_val_var_id ON var (id);


----------------------------------------------------------------------------------------------------
/*
CREATE TABLE var_constraint (
  id BIGSERIAL PRIMARY KEY NOT NULL,
  created timestamp NOT NULL DEFAULT now(),
  var_id bigint NOT NULL REFERENCES form_item(id) ON DELETE CASCADE ON UPDATE CASCADE,
  note text,
  UNIQUE(exp_id, name)
);
*/


----------------------------------------------------------------------------------------------------
/*
CREATE TABLE var_set (
  id BIGSERIAL PRIMARY KEY NOT NULL,
  created timestamp NOT NULL DEFAULT now(),
  exp_id bigint NOT NULL REFERENCES exp(id) ON DELETE CASCADE ON UPDATE CASCADE,
  name varchar(64),
  txt_top text,
  lbl_btn_prev varchar(255),
  lbl_btn_next varchar(255),
  note text,
  UNIQUE(exp_id, name)
);

DROP INDEX IF EXISTS form_exp_id;  CREATE INDEX form_exp_id ON exp (id);
*/


----------------------------------------------------------------------------------------------------
CREATE TABLE cond (
  id BIGSERIAL PRIMARY KEY NOT NULL,
  created timestamp NOT NULL DEFAULT now(),
  exp_id bigint NOT NULL REFERENCES exp(id) ON DELETE CASCADE ON UPDATE CASCADE,
  name varchar(128) NOT NULL,
  idx smallint CHECK (idx >= 0),
  out_init numeric(12,6) NOT NULL DEFAULT 0,
  sub_cnt_max integer CHECK (sub_cnt_max >= 0),
  timeout smallint NOT NULL DEFAULT 600 CHECK (timeout > 0),
  amt_pay_bonus_max smallint NOT NULL DEFAULT 0 CHECK (amt_pay_bonus_max >= 0),
  amt_pay_bonus_mult numeric(12,6) NOT NULL DEFAULT 1,
  note text,
  UNIQUE(exp_id, name),
  UNIQUE(exp_id, idx)
);

COMMENT ON COLUMN cond.out_init           IS 'The initial outcome (or money) with which a subjects assigned to this condition starts';
COMMENT ON COLUMN cond.sub_cnt_max        IS 'Number of subjects to be assigned to the condition';
COMMENT ON COLUMN cond.amt_pay_bonus_max  IS 'The maximum bonus a subject can possibly get through the Amazon Mechanical Turk by completing this condition. This is a security mechanism that prevents automatic reward granting if that reward would be higher than the out_amt_max value. [cents]';
COMMENT ON COLUMN cond.amt_pay_bonus_mult IS 'The outcome multiplier to calculate the bonus amount payed to a subject through the Amazon Mechanical Turk (the final amount of in-game currency multiplied by this factor yields the actual monetary bonus which is later added to out_amt_base)';
COMMENT ON COLUMN cond.timeout            IS 'Inactivity time after which all user sessions associated with this condition will be invalidated [s]';

DROP INDEX IF EXISTS cond_exp_id;  CREATE INDEX cond_exp_id ON exp (id);


----------------------------------------------------------------------------------------------------
CREATE TABLE block (
  id BIGSERIAL PRIMARY KEY NOT NULL,
  created timestamp NOT NULL DEFAULT now(),
  cond_id bigint NOT NULL REFERENCES cond(id) ON DELETE CASCADE ON UPDATE CASCADE,
  name varchar(128) NOT NULL,
  idx smallint CHECK (idx >= 0),
  trial_cnt smallint CHECK (trial_cnt >= 0),
  model_prob_id bigint REFERENCES model_prob(id) ON DELETE CASCADE ON UPDATE CASCADE,
  model_ibl_id bigint REFERENCES model_ibl(id) ON DELETE CASCADE ON UPDATE CASCADE,
  html_id bigint REFERENCES html(id) ON DELETE CASCADE ON UPDATE CASCADE,
  var_id bigint REFERENCES var(id) ON DELETE CASCADE ON UPDATE CASCADE,
  do_upd_out boolean NOT NULL DEFAULT false,
  do_rand_dec_ord boolean NOT NULL DEFAULT true,
  note text,
  UNIQUE(cond_id, name),
  UNIQUE(cond_id, idx)
);

COMMENT ON COLUMN block.trial_cnt       IS 'Number of trials in a block (0 indicated that subject is free to end this block at any time)';
COMMENT ON COLUMN block.do_upd_out      IS 'Update the in-game currency and Amazon Mechanical Turk outcome with the activity in this block?';
COMMENT ON COLUMN block.do_rand_dec_ord IS 'Randomize the left-to-right decision order when presenting the game to the subject?';

DROP INDEX IF EXISTS block_cond_id;        CREATE INDEX block_cond_id       ON cond       (id);
DROP INDEX IF EXISTS block_model_prob_id;  CREATE INDEX block_model_prob_id ON model_prob (id);
DROP INDEX IF EXISTS block_model_ibl_id;   CREATE INDEX block_model_ibl_id  ON model_ibl  (id);
DROP INDEX IF EXISTS block_html_id;        CREATE INDEX block_html_id       ON html       (id);
DROP INDEX IF EXISTS block_var_id;         CREATE INDEX block_var_id        ON var        (id);


----------------------------------------------------------------------------------------------------
/*
CREATE TABLE payoff
(
  id BIGSERIAL PRIMARY KEY NOT NULL,
  block_id bigint NOT NULL REFERENCES action(id) ON DELETE CASCADE ON UPDATE CASCADE,
  coord_row smallint CHECK (coord_row >= 0),
  coord_col smallint CHECK (coord_col >= 0),
  out numeric(12,6) NOT NULL,
  UNIQUE(block_id, coord_row, coord_col)
);

COMMENT ON COLUMN payoff.out IS 'Outcome';

DROP INDEX IF EXISTS payoff_action_id;  CREATE INDEX payoff_action ON action (id);
*/


----------------------------------------------------------------------------------------------------
CREATE TABLE sess (
  id BIGSERIAL PRIMARY KEY NOT NULL,
  created timestamp NOT NULL DEFAULT now(),
  sub_id bigint NOT NULL REFERENCES sub(id) ON DELETE CASCADE ON UPDATE CASCADE,
  cond_id bigint NOT NULL REFERENCES cond(id) ON DELETE CASCADE ON UPDATE CASCADE,
  amt_hit_id varchar(128),
  amt_assign_id varchar(128),
  is_ended boolean NOT NULL DEFAULT false,
  is_valid boolean NOT NULL DEFAULT false
);

COMMENT ON COLUMN sess.amt_hit_id    IS 'Amazon Mechanical Turk - HIT ID';
COMMENT ON COLUMN sess.amt_assign_id IS 'Amazon Mechanical Turk - Assignment ID';
COMMENT ON COLUMN sess.is_ended      IS 'Has the session ended? A session can end normally (i.e., the subject completes the condition) or abnormally (e.g., the subject closes the broswer).';
COMMENT ON COLUMN sess.is_valid      IS 'Is the session valid? An invalid session is one that has not been completed entirely (e.g., due to the subject closing their browser prior to concluding all the blocks within the condition they have been assigned to).';

DROP INDEX IF EXISTS sess_cond_id;  CREATE INDEX sess_cond_id ON cond (id);
DROP INDEX IF EXISTS sess_sub_id;   CREATE INDEX sess_sub_id  ON sub  (id);


----------------------------------------------------------------------------------------------------
CREATE TABLE sess_var (
  id BIGSERIAL PRIMARY KEY NOT NULL,
  created timestamp NOT NULL DEFAULT now(),
  sess_id bigint NOT NULL REFERENCES sess(id) ON DELETE CASCADE ON UPDATE CASCADE,
  var_id bigint NOT NULL REFERENCES var(id) ON DELETE CASCADE ON UPDATE CASCADE,
  val text
);

DROP INDEX IF EXISTS sess_var_sess_id;  CREATE INDEX sess_var_sess_id ON sess (id);
DROP INDEX IF EXISTS sess_var_var_id;   CREATE INDEX sess_var_var_id  ON var  (id);


----------------------------------------------------------------------------------------------------
CREATE TABLE trial (
  id BIGSERIAL PRIMARY KEY NOT NULL,
  created timestamp NOT NULL DEFAULT now(),
  sess_id bigint NOT NULL REFERENCES sess(id) ON DELETE CASCADE ON UPDATE CASCADE,
  block_id bigint NOT NULL REFERENCES block(id) ON DELETE CASCADE ON UPDATE CASCADE,
  idx_sess smallint CHECK (idx_sess >= 0),
  idx_block smallint CHECK (idx_block >= 0),
  dec_ord char(3) NOT NULL,
  dec_sub_t bigint NOT NULL DEFAULT 0 CHECK (dec_sub_t >= 0),
  dec_sub smallint CHECK (dec_sub >= 0),
  dec_mod smallint CHECK (dec_mod >= 0),
  out_sub numeric(12,6),
  out_sub_tot_before numeric(12,6),
  out_sub_tot_after numeric(12,6),
  out_mod numeric(12,6),
  out_mod_tot_before numeric(12,6),
  out_mod_tot_after numeric(12,6),
  --mod_eq_a double precision,
  --mod_eq_p double precision,
  --mod_eq_v double precision,
  UNIQUE(sess_id, idx_sess),
  UNIQUE(sess_id, block_id, idx_block)
);

COMMENT ON COLUMN trial.idx_sess  IS 'Index within the session';
COMMENT ON COLUMN trial.idx_block IS 'Index within the block';
COMMENT ON COLUMN trial.dec_sub_t IS 'Time it took the subject to make a decision (i.e., the time from the decision presentation to the button press) [ms]';
COMMENT ON COLUMN trial.dec_ord   IS 'Left-to-right order of decisions as presented to the subject';
COMMENT ON COLUMN trial.dec_sub   IS 'Decision of the subject';
COMMENT ON COLUMN trial.dec_mod   IS 'Decision of the model';
--COMMENT ON COLUMN trial.mod_eq_a IS 'Model equation value: Activation';
--COMMENT ON COLUMN trial.mod_eq_p IS 'Model equation value: Retrieval probability';
--COMMENT ON COLUMN trial.mod_eq_v IS 'Model equation value: Blended value';

DROP INDEX IF EXISTS trial_sess_id;   CREATE INDEX trial_sess_id   ON sess  (id);
DROP INDEX IF EXISTS trial_block_id;  CREATE INDEX trial_block_id  ON block (id);


----------------------------------------------------------------------------------------------------
COMMIT;


----------------------------------------------------------------------------------------------------
-- Info --

-- smallint         : 2B  (-32768 to +32767)
-- integer          : 4B  (-2147483648 to +2147483647)
-- real             : 4B  (6 decimal digits precision) [variable-precision, inexact]
-- double precision : 8B  (15 decimal digits precision) [variable-precision, inexact]

-- PostgreSQL keywords: http://www.postgresql.org/docs/8.3/static/sql-keywords-appendix.html
