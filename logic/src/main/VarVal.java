package main;


/**
 * @author Tomek D. Loboda
 */
public class VarVal {
	private final long   id;
	private final String val;
	private final int    idx;
	private final String lbl;

	
	// -----------------------------------------------------------------------------------------------------------------
	public VarVal(long id, String val, int idx, String lbl) {
		this.id  = id;
		this.val = val;
		this.idx = idx;
		this.lbl = lbl;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public long   getId()  { return id;  }
	public String getVal() { return val; }
	public long   getIdx() { return idx; }
	public String getLbl() { return lbl; }
}
