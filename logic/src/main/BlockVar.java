package main;

import inter.DBI;
import inter.JSI;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import util.$;


/**
 * @author Tomek D. Loboda
 */
public class BlockVar {
	public static enum FormItemType { UNKNOWN, TXT_LINE, TXT_BLOCK , OPTIONS_ONE, OPTIONS_MULTIPLE };
	
	private final long varId;
	private final String name;
	private final String txtLbl;
	private final String txtHelp;
	
	private final FormItemType type;
	
	private final boolean isReq;
	
	//private final int maxRespTime;  // [s]
	
	private final List<VarVal> lstVal = new ArrayList<VarVal>();
	private final String uiVal;
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public BlockVar(long varId)  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		DBI dbi = null;
		try {
			dbi = new DBI();
			
			this.varId = varId;
			
			// (1) Form item:
			ResultSet rsVar = dbi.execQry("SELECT id, name, type, txt_lbl, txt_help, is_req FROM var WHERE id = " + varId);
			rsVar.next();
			
			varId = rsVar.getLong("id");
			
			String tmp = null;
			tmp = rsVar.getString("name");     name    = (rsVar.wasNull() ? null : tmp);
			tmp = rsVar.getString("txt_lbl");  txtLbl  = (rsVar.wasNull() ? null : tmp);
			tmp = rsVar.getString("txt_help"); txtHelp = (rsVar.wasNull() ? null : tmp);
			
			tmp = rsVar.getString("type");
			     if (tmp.equals("txt-line"))         type = FormItemType.TXT_LINE;
			else if (tmp.equals("txt-block"))        type = FormItemType.TXT_BLOCK;
			else if (tmp.equals("options-one"))      type = FormItemType.OPTIONS_ONE;
			else if (tmp.equals("options-multiple")) type = FormItemType.OPTIONS_MULTIPLE;
			else type = FormItemType.UNKNOWN;
			
			isReq = rsVar.getBoolean("is_req");
			
			//maxRespTime = rs.getInt("max_resp_time");
			
			rsVar.close();
			
			// (2) Options:
			if (type == FormItemType.OPTIONS_ONE || type == FormItemType.OPTIONS_MULTIPLE) {
				StringBuilder sbVal = new StringBuilder("[");
				
				ResultSet rsVal = dbi.execQry("SELECT id, val, idx, txt_lbl FROM var_val WHERE var_id = " + varId + " ORDER BY idx");
				while (rsVal.next()) {
					VarVal vv = new VarVal(rsVal.getLong("id"), rsVal.getString("val"), rsVal.getInt("idx"), rsVal.getString("txt_lbl"));
					lstVal.add(vv);
					sbVal.append("{id:" + vv.getId() + ",val:" + JSI.str2js(vv.getVal()) + ",idx:" + vv.getIdx() + ",lbl:" + JSI.str2js(vv.getLbl()) + "}" + (rsVal.isLast() ? "" : ","));
				}
				rsVal.close();
				
				sbVal.append("]");
				uiVal = sbVal.toString();
			}
			else {
				uiVal = null;
			}
		}
		finally {
			if (dbi != null) dbi.close();
		}
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public String getName() { return name; }
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public String getUIActions() {
		List<String> res = new ArrayList<String>();
		
		res.add(Sess.CLIENT_ACT_BLOCK_SET_VAR);
		
		switch (type) {
			case TXT_LINE:
				res.add(Sess.CLIENT_ACT_VAR_SET + "(" + JSI.str2js(name) + ",'txt-line'," + isReq + "," + JSI.str2js(txtLbl) + "," + JSI.str2js(txtHelp) + ", false, null)");
				break;
				
			case TXT_BLOCK:
				res.add(Sess.CLIENT_ACT_VAR_SET + "(" + JSI.str2js(name) + ",'txt-block'," + "," + isReq + JSI.str2js(txtLbl) + "," + JSI.str2js(txtHelp) + ", false, null)");
				break;
				
			case OPTIONS_ONE:
				res.add(Sess.CLIENT_ACT_VAR_SET + "(" + JSI.str2js(name) + ",'options-one'," + isReq + "," + JSI.str2js(txtLbl) + "," + JSI.str2js(txtHelp) + ", false, " + uiVal + ")");
				break;
				
			case OPTIONS_MULTIPLE:
				res.add(Sess.CLIENT_ACT_VAR_SET + "(" + JSI.str2js(name) + ",'options-multiple'," + isReq + "," + JSI.str2js(txtLbl) + "," + JSI.str2js(txtHelp) + ", true, " + uiVal + ")");
				break;
		}
		
		res.add(Sess.CLIENT_ACT_APP_BTN_PREV_LBL_SET + "(null)");
		res.add(Sess.CLIENT_ACT_APP_BTN_NEXT_LBL_SET + "('Continue')");
		
		return $.join(res, "", "", ",", false, true).toString();
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public void persist(long sessId, String val)  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		DBI dbi = null;
		try {
			dbi = new DBI();
			dbi.execUpd("INSERT INTO sess_var (sess_id, var_id, val) VALUES (" + sessId + "," + varId + "," + DBI.esc(val) + ")");
		}
		finally {
			if (dbi != null) dbi.close();
		}
	}
}
