package main;

import inter.DBI;
import inter.JSI;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import util.$;
import exception.LogicException;


/**
 * @author Tomek D. Loboda
 */
public class Exp {
	public static final String ERR_EXP_NOT_FOUND = "The specified experiment does not exist or you do not have sufficient access rights.";
	
	public static final String VAR_NAME_SEP_DEF = "-";
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Consolidates the specified experiment by ensuring that:
	 * 
	 * - All sessions that has not ended properly are marked as invalid
	 * 
	 * This method should be called after the experiment has been fully recruited for to ensure that unforeseen 
	 * anomalies did not corrupt its state. A system health check routine is run both on startup and shutdown and this 
	 * method is only to ensure no problems in the interim.
	 */
	public static Result consolidate(DBI dbi, long expId) {
		// ...
		
		return new Result(true, "");
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static void checkIdValid(long expId)  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException, LogicException {
		if (DBI.getLong((DBI) null, "SELECT COUNT(*) FROM exp WHERE id = " + expId) == 0) throw new LogicException(ERR_EXP_NOT_FOUND);
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static Result getInf(long expId, boolean isSim)  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		Result res = null;
		DBI dbi = null;
		
		try {
			dbi = new DBI();
			String qryCondLst = Cond.getQry_lst(expId, isSim);
			
			ResultSet rsExp = dbi.execQry("SELECT name, amt_assign_cnt, amt_pay_base, amt_frame_h, amt_t_exp, amt_t_sess FROM exp WHERE id = " + expId);
			
			if (!rsExp.next()) {
				res = new Result(false, "msg:\"" + ERR_EXP_NOT_FOUND + "\"");
			}
			else {
				// Get the max and current number of subjects:
				ResultSet rsTmp = dbi.execQry("SELECT SUM(sub_cnt_max) AS sub_cnt_max, SUM(sub_cnt_curr) AS sub_cnt_curr FROM (" + qryCondLst + ") AS a");
				rsTmp.next();
				int subCntMax  = rsTmp.getInt("sub_cnt_max");
				int subCntCurr = rsTmp.getInt("sub_cnt_curr");
				rsTmp.close();
				
				// Generate the result:
				int amtPayBase = rsExp.getInt("amt_pay_base");
				StringBuilder sb = new StringBuilder("exp:{");
				sb.append(
					"id:"           + expId                                                     + "," +
					"name:"         + JSI.str2js(rsExp.getString("name"))                       + "," +
					"subCntMax:"    + subCntMax                                                 + "," +
					"subCntCurr:"   + subCntCurr                                                + "," +
					"amtAssignCnt:" + rsExp.getInt("amt_assign_cnt")                            + "," +
					"amtPayBase:"   + rsExp.getInt("amt_pay_base")                              + "," +
					"amtFrameH:"    + rsExp.getInt("amt_frame_h")                               + "," +
					"amtTExp:"      + rsExp.getLong("amt_t_exp")                                + "," +
					"amtTSess:"     + rsExp.getLong("amt_t_sess")                               + "," +
					"costTotal:"    + (((double) amtPayBase * subCntMax) / 100d)                + "," +
					"costDone:"     + (((double) amtPayBase * subCntCurr) / 100d)               + "," +
					"costLeft:"     + (((double) amtPayBase * (subCntMax - subCntCurr)) / 100d) + "," +
					"cond:["
				);
				
				ResultSet rsCond = dbi.execQry(qryCondLst);
				while (rsCond.next()) {
					sb.append(
						"{" +
						"id:"              + rsCond.getLong("id")                   + "," + 
						"name:"            + JSI.str2js(rsCond.getString("name"))   + "," +
						"idx:"             + rsCond.getInt("idx")                   + "," +
						"outInit:"         + rsCond.getDouble("out_init")           + "," +
						"amtPayBonusMax:"  + rsCond.getInt("amt_pay_bonus_max")     + "," +
						"amtPayBonusMult:" + rsCond.getDouble("amt_pay_bonus_mult") + "," +
						"subCntMax:"       + rsCond.getInt("sub_cnt_max")           + "," +
						"subCntCurr:"      + rsCond.getInt("sub_cnt_curr")          +
						"}" +
						(!rsCond.isLast() ? "," : "")
					);
				}
				rsCond.close();
				
				sb.append("]");
				sb.append("}");
				
				res = new Result(true, sb);
			}
			rsExp.close();
		}
		finally {
			if (dbi != null) dbi.close();
		}
		
		return res;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static Result getLst()  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		StringBuilder res = new StringBuilder("exp:[");
		DBI dbi = null;
		
		try {
			dbi = new DBI();
			
			ResultSet rs = dbi.execQry("SELECT id, name FROM exp");
			
			while (rs.next()) {
				res.append(
					"{" +
					"id:" + rs.getInt("id") + "," +
					"name:" + JSI.str2js(rs.getString("name")) + "," +
					"}"
				);
				if (!rs.isLast()) res.append(",");
			}
			rs.close();
			res.append("]");
		}
		finally {
			if (dbi != null) dbi.close();
		}
		
		return new Result(true, res);
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static StringBuilder getRep(long expId, boolean isSim, boolean isSessEnded, String varNameSep, String missVal)  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		StringBuilder res = new StringBuilder();
		DBI dbi = null;
		
		try {
			dbi = new DBI();
			
			if (varNameSep == null) varNameSep = VAR_NAME_SEP_DEF;
			if (missVal == null) missVal = ".";
			
			// (0) Init:
			res.append(
				"<!DOCTYPE html>" +
				"<html>" +
				"<head>" +
				"<link rel=\"stylesheet\" type=\"text/css\" href=\"style/exp-rep.css\" />" +
				"</head>" +
				"<body>"
			);
			
			// (1) Report and experiment:
			ResultSet rsExp = dbi.execQry("SELECT name FROM exp WHERE id = " + expId);
			rsExp.next();
			res.append(
				"<div class=\"sec\">1. Report and experiment</div>" +
				"<table cellpadding=\"0\" cellspacing=\"0\" class=\"v\">" +
				"<tr><th>Report generated (all times are server time)</th><td>:</td><td>" + $.date2str(new Date()) + "</td></tr>" +
				"</table>" +
				"<br />" +
				"<table cellpadding=\"0\" cellspacing=\"0\" class=\"v\">" +
				"<tr><th>Experiment name</th><td>:</td><td>" + rsExp.getString("name") + "</td></tr>" +
				"<tr><th>Experiment ID</th><td>:</td><td>" + expId + "</td></tr>" +
				"<tr><th>Subjects included</th><td>:</td><td>" + (isSim ? "Simulated" : "Real") + "</td></tr>" +
				"<tr><th>Sessions included</th><td>:</td><td>" + (isSessEnded ? "Ended (some may be invalid)" : "Live (all of which are invalid until completed)") + "</td></tr>" +
				"</table>" +
				"<br />" +
				"<table cellpadding=\"0\" cellspacing=\"0\" class=\"v\">" +
				"<tr><th>Subject count</th><td>:</td><td>" +  DBI.getLong(dbi, "SELECT COUNT(*) AS cnt FROM sub su      INNER JOIN sess se ON se.sub_id = su.id  INNER JOIN cond c ON c.id = se.cond_id                                        WHERE c.exp_id = " + expId + " AND su.is_sim = " + isSim + " AND se.is_ended = " + isSessEnded) + "</td></tr>" +
				"<tr><th>Session count</th><td>:</td><td>" +  DBI.getLong(dbi, "SELECT COUNT(*) AS cnt FROM sess se                                              INNER JOIN cond c ON c.id = se.cond_id INNER JOIN sub su ON su.id = se.sub_id WHERE c.exp_id = " + expId + " AND su.is_sim = " + isSim + " AND se.is_ended = " + isSessEnded) + "</td></tr>" +
				"<tr><th>Trial count</th><td>:</td><td>" +    DBI.getLong(dbi, "SELECT COUNT(*) AS cnt FROM trial t     INNER JOIN sess se ON se.id = t.sess_id  INNER JOIN cond c ON c.id = se.cond_id INNER JOIN sub su ON su.id = se.sub_id WHERE c.exp_id = " + expId + " AND su.is_sim = " + isSim + " AND se.is_ended = " + isSessEnded) + "</td></tr>" +
				"<tr><th>Variable count</th><td>:</td><td>" + DBI.getLong(dbi, "SELECT COUNT(*) AS cnt FROM sess_var sv INNER JOIN sess se ON se.id = sv.sess_id INNER JOIN cond c ON c.id = se.cond_id INNER JOIN sub su ON su.id = se.sub_id WHERE c.exp_id = " + expId + " AND su.is_sim = " + isSim + " AND se.is_ended = " + isSessEnded) + "</td></tr>" +
				"</table>"
			);
			rsExp.close();
			
			
			// (2) Subjects:
			res.append(
				"<br />" +
				"<br />" +
				"<div class=\"sec\">2. Subjects</div>" +
				"<table cellpadding=\"0\" cellspacing=\"0\" class=\"h\">" +
				"<tr><th>sub" + varNameSep + "id</th><th>sub" + varNameSep + "created</th><th>uname</th><th>amt" + varNameSep + "worker" + varNameSep + "id</th><th>is" + varNameSep + "sim</th><th>sess" + varNameSep + "cnt</th></tr>"
			);
			
			ResultSet rsSub = dbi.execQry(
				"SELECT " +
					"su.id AS sub_id, " +
					"su.created AS sub_created, " +
					"COALESCE(su.uname, '" + missVal + "') AS uname, " +
					"su.is_sim, " +
					"COALESCE(su.amt_worker_id, '" + missVal + "') AS amt_worker_id, " +
					"COUNT(se.id) AS sess_cnt " +
				"FROM sub su " +
				"INNER JOIN sess se ON se.sub_id = su.id " +
				"WHERE su.exp_id = " + expId + " AND su.is_sim = " + isSim + " AND se.is_ended = " + isSessEnded + " " +
				"GROUP BY su.id, su.created, su.uname, su.is_sim, su.amt_worker_id " +
				"ORDER BY su.id"
			);
			
			while (rsSub.next()) {
				res.append(
					"<tr>" +
					"<td>" + rsSub.getLong("sub_id")                       + "</td>" +
					"<td>" + $.date2str(rsSub.getTimestamp("sub_created")) + "</td>" +
					"<td>" + rsSub.getString("uname")                      + "</td>" +
					"<td>" + rsSub.getString("amt_worker_id")              + "</td>" +
					"<td>" + (rsSub.getBoolean("is_sim") ? "1" : "0")      + "</td>" +
					"<td>" + rsSub.getInt("sess_cnt")                      + "</td>" +
					"</tr>"
				);
			}
			
			rsSub.close();
			res.append("</table>");
			
			
			// (3) Sessions:
			res.append(
				"<br />" +
				"<br />" +
				"<div class=\"sec\">3. Sessions</div>" +
				"<table cellpadding=\"0\" cellspacing=\"0\" class=\"h\">" +
				"<tr><th>sess" + varNameSep + "id</th><th>sess" + varNameSep + "created</th><th>sess" + varNameSep + "valid</th><th>sub" + varNameSep + "id</th><th>cond" + varNameSep + "id</th><th>amt" + varNameSep + "hit" + varNameSep + "id</th><th>amt" + varNameSep + "assign" + varNameSep + "id</th><th>trial" + varNameSep + "cnt</th><th>var" + varNameSep + "val" + varNameSep + "cnt</th></tr>"
			);
			
			ResultSet rsSess = dbi.execQry(
				"SELECT " +
					"se.id AS sess_id, " +
					"se.created AS sess_created, " +
					"se.is_valid AS sess_valid, " +
					"COALESCE(se.amt_hit_id, '" + missVal + "') AS amt_hit_id, " +
					"COALESCE(se.amt_assign_id, '" + missVal + "') AS amt_assign_id, " +
					"su.id AS sub_id, " +
					"c.id AS cond_id, " +
					"tmp01.trial_cnt, " +
					"tmp02.var_cnt " +
				"FROM sess se " +
				"INNER JOIN sub su ON su.id = se.sub_id " +
				"INNER JOIN cond c ON c.id = se.cond_id " +
				"LEFT JOIN (SELECT t.sess_id, COUNT(*) as trial_cnt FROM trial t INNER JOIN sess se ON se.id = t.sess_id INNER JOIN cond c ON c.id = se.cond_id WHERE c.exp_id = " + expId + " GROUP BY t.sess_id) AS tmp01 ON tmp01.sess_id = se.id " +
				"LEFT JOIN (SELECT sv.sess_id, COUNT(*) as var_cnt FROM sess_var sv INNER JOIN sess se ON se.id = sv.sess_id INNER JOIN cond c ON c.id = se.cond_id WHERE c.exp_id = " + expId + " GROUP BY sv.sess_id) AS tmp02 ON tmp02.sess_id = se.id " +
				"WHERE c.exp_id = " + expId + " AND su.is_sim = " + isSim + " AND se.is_ended = " + isSessEnded + " " +
				"ORDER BY se.id"
			);
			
			while (rsSess.next()) {
				res.append(
					"<tr>" +
					"<td>" + rsSess.getLong("sess_id")                       + "</td>" +
					"<td>" + $.date2str(rsSess.getTimestamp("sess_created")) + "</td>" +
					"<td>" + (rsSess.getBoolean("sess_valid") ? "1" : "0")   + "</td>" +
					"<td>" + rsSess.getLong("sub_id")                        + "</td>" +
					"<td>" + rsSess.getLong("cond_id")                       + "</td>" +
					"<td>" + rsSess.getString("amt_hit_id")                  + "</td>" +
					"<td>" + rsSess.getString("amt_assign_id")               + "</td>" +
					"<td>" + rsSess.getInt("trial_cnt")                      + "</td>" +
					"<td>" + rsSess.getInt("var_cnt")                        + "</td>" +
					"</tr>"
				);
			}
			
			rsSess.close();
			res.append("</table>");
			
			
			// (4) Trials:
			// (4.1) Variable names:
			int varCnt = DBI.getLong(dbi, "SELECT COUNT(*) FROM var WHERE exp_id = " + expId).intValue();
			StringBuilder varNames = new StringBuilder();
			
			ResultSet rsTrialVarName = dbi.execQry(
				"SELECT " +
					"v.name AS var_name " +
				"FROM var v " +
				"WHERE v.exp_id = " + expId + " " +
				"ORDER BY v.id"
			);
			while (rsTrialVarName.next()) varNames.append("<th>var" + varNameSep + rsTrialVarName.getString("var_name") + "</th>");
			rsTrialVarName.close();
			
			// (4.2) Section and table header:
			res.append(
				"<br />" +
				"<br />" +
				"<div class=\"sec\">4. Trials</div>" +
				"<table cellpadding=\"0\" cellspacing=\"0\" class=\"h\">" +
				"<tr><th>trial" + varNameSep + "id</th><th>trial" + varNameSep + "created</th><th>sess" + varNameSep + "id</th><th>sess" + varNameSep + "created</th><th>sess" + varNameSep + "valid</th><th>sub" + varNameSep + "id</th><th>cond" + varNameSep + "id</th><th>cond" + varNameSep + "name</th><th>block" + varNameSep + "id</th><th>block" + varNameSep + "name</th><th>trial" + varNameSep + "idx" + varNameSep + "sess</th><th>trial" + varNameSep + "idx" + varNameSep + "block</th><th>dec" + varNameSep + "ord</th><th>dec" + varNameSep + "sub" + varNameSep + "t</th><th>dec" + varNameSep + "sub</th><th>dec" + varNameSep + "mod</th><th>out" + varNameSep + "sub</th><th>out" + varNameSep + "sub" + varNameSep + "tot" + varNameSep + "b</th><th>out" + varNameSep + "sub" + varNameSep + "tot" + varNameSep + "a</th><th>out" + varNameSep + "mod</th><th>out" + varNameSep + "mod" + varNameSep + "tot" + varNameSep + "b</th><th>out" + varNameSep + "mod" + varNameSep + "tot" + varNameSep + "a</th><th>amt" + varNameSep + "hit" + varNameSep + "id</th><th>amt" + varNameSep + "worker" + varNameSep + "id</th><th>amt" + varNameSep + "assign" + varNameSep + "id</th>" + varNames.toString() + "</tr>"
			);
			
			// (4.3) Table body:
			ResultSet rsTrial = dbi.execQry(
				"SELECT " +
					"t.id AS trial_id, " +
					"t.created AS trial_created, " +
					"se.id AS sess_id, " +
					"se.created AS sess_created, " +
					"se.is_valid AS sess_valid, " +
					"COALESCE(se.amt_hit_id, '" + missVal + "') AS amt_hit_id, " +
					"COALESCE(se.amt_assign_id, '" + missVal + "') AS amt_assign_id, " +
					"su.id AS sub_id, " +
					"COALESCE(su.amt_worker_id, '" + missVal + "') AS amt_worker_id, " +
					"c.id AS cond_id, " +
					"c.name AS cond_name, " +
					"t.block_id, " +
					"b.name AS block_name, " +
					"t.idx_sess, " +
					"t.idx_block, " +
					"t.dec_ord, " +
					"t.dec_sub_t, " +
					"COALESCE(CASE WHEN t.dec_sub = 0 THEN '0' ELSE t.dec_sub::text END, '" + missVal + "') AS dec_sub, " +
					"COALESCE(CASE WHEN t.dec_mod = 0 THEN '0' ELSE t.dec_mod::text END, '" + missVal + "') AS dec_mod, " +
					"COALESCE(CASE WHEN t.out_sub            = 0 THEN '0' ELSE trim(trailing '.' from trim(trailing '0' from t.out_sub::text))            END, '" + missVal + "') AS out_sub, "            +
					"COALESCE(CASE WHEN t.out_sub_tot_before = 0 THEN '0' ELSE trim(trailing '.' from trim(trailing '0' from t.out_sub_tot_before::text)) END, '" + missVal + "') AS out_sub_tot_before, " +
					"COALESCE(CASE WHEN t.out_sub_tot_after  = 0 THEN '0' ELSE trim(trailing '.' from trim(trailing '0' from t.out_sub_tot_after::text))  END, '" + missVal + "') AS out_sub_tot_after, "  +
					"COALESCE(CASE WHEN t.out_mod            = 0 THEN '0' ELSE trim(trailing '.' from trim(trailing '0' from t.out_mod::text))            END, '" + missVal + "') AS out_mod, "            +
					"COALESCE(CASE WHEN t.out_mod_tot_before = 0 THEN '0' ELSE trim(trailing '.' from trim(trailing '0' from t.out_mod_tot_before::text)) END, '" + missVal + "') AS out_mod_tot_before, " +
					"COALESCE(CASE WHEN t.out_mod_tot_after  = 0 THEN '0' ELSE trim(trailing '.' from trim(trailing '0' from t.out_mod_tot_after::text))  END, '" + missVal + "') AS out_mod_tot_after "   +
				"FROM trial t " +
				"INNER JOIN block b ON b.id = t.block_id " +
				"INNER JOIN cond c ON c.id = b.cond_id " +
				"INNER JOIN sess se ON se.id = t.sess_id " +
				"INNER JOIN sub su ON su.id = se.sub_id " +
				"WHERE c.exp_id = " + expId + " AND su.is_sim = " + isSim + " AND se.is_ended = " + isSessEnded + " " +
				"ORDER BY se.id, t.id"
			);
			
			while (rsTrial.next()) {
				long sessId = rsTrial.getLong("sess_id");
				
				// (4.3.1) Trial, session, subject, condition, and block info:
				res.append(
					"<tr>" +
					"<td>" + rsTrial.getLong("trial_id")                       + "</td>" +
					"<td>" + $.date2str(rsTrial.getTimestamp("trial_created")) + "</td>" +
					"<td>" + sessId                                            + "</td>" +
					"<td>" + $.date2str(rsTrial.getTimestamp("sess_created"))  + "</td>" +
					"<td>" + (rsTrial.getBoolean("sess_valid") ? "1" : "0")    + "</td>" +
					"<td>" + rsTrial.getLong("sub_id")                         + "</td>" +
					"<td>" + rsTrial.getLong("cond_id")                        + "</td>" +
					"<td>" + rsTrial.getString("cond_name")                    + "</td>" +
					"<td>" + rsTrial.getLong("block_id")                       + "</td>" +
					"<td>" + rsTrial.getString("block_name")                   + "</td>" +
					"<td>" + rsTrial.getInt("idx_sess")                        + "</td>" +
					"<td>" + rsTrial.getInt("idx_block")                       + "</td>" +
					"<td>" + rsTrial.getString("dec_ord")                      + "</td>" +
					"<td>" + rsTrial.getLong("dec_sub_t")                      + "</td>" +
					"<td>" + rsTrial.getString("dec_sub")                      + "</td>" +
					"<td>" + rsTrial.getString("dec_mod")                      + "</td>" +
					"<td>" + rsTrial.getString("out_sub")                      + "</td>" +
					"<td>" + rsTrial.getString("out_sub_tot_before")           + "</td>" +
					"<td>" + rsTrial.getString("out_sub_tot_after")            + "</td>" +
					"<td>" + rsTrial.getString("out_mod")                      + "</td>" +
					"<td>" + rsTrial.getString("out_mod_tot_before")           + "</td>" +
					"<td>" + rsTrial.getString("out_mod_tot_after")            + "</td>" +
					"<td>" + rsTrial.getString("amt_hit_id")                   + "</td>" +
					"<td>" + rsTrial.getString("amt_worker_id")                + "</td>" +
					"<td>" + rsTrial.getString("amt_assign_id")                + "</td>"
				);
				
				// (4.3.2) Variable values:
				ResultSet rsTrialVarVal = dbi.execQry(
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					"SELECT " +
						"v.id AS var_id, " +
						"COALESCE(sv.val, '') AS var_val " +
					"FROM var v " +
					"LEFT JOIN sess_var sv ON sv.var_id = v.id " +
					"LEFT JOIN sess se ON se.id = sv.sess_id " +
					"LEFT JOIN sub su ON su.id = se.sub_id " +
					"WHERE v.exp_id = " + expId + " AND se.id = " + sessId + " " +
					"ORDER BY v.id"
				);
				if (DBI.getRecCnt(rsTrialVarVal) == 0) {
					res.append($.strRep("<td></td>", varCnt));
				}
				else while (rsTrialVarVal.next()) res.append("<td>" + rsTrialVarVal.getString("var_val") + "</td>");
				rsTrialVarVal.close();
				
				res.append("</tr>");
			}
			
			rsTrial.close();
			res.append("</table>");
			
			
			// (5) Variables:
			res.append(
				"<br />" +
				"<br />" +
				"<div class=\"sec\">5. Variables</div>" +
				"<table cellpadding=\"0\" cellspacing=\"0\" class=\"h\">" +
				"<tr><th>sess" + varNameSep + "id</th><th>sess" + varNameSep + "created</th><th>sess" + varNameSep + "valid</th><th>sub" + varNameSep + "id</th><th>var" + varNameSep + "id</th><th>var" + varNameSep + "name</th><th>var" + varNameSep + "val</th><th>var" + varNameSep + "val" + varNameSep + "created</th></tr>"
			);
			
			ResultSet rsVar = dbi.execQry(
				"SELECT " +
					"se.id AS sess_id, " +
					"se.created AS sess_created, " +
					"se.is_valid AS sess_valid, " +
					"su.id AS sub_id, " +
					"v.id AS var_id, " +
					"v.name AS var_name, " +
					"sv.val AS var_val, " +
					"sv.created AS var_val_created " +
				"FROM sess_var sv " +
				"INNER JOIN var v ON v.id = sv.var_id " +
				"INNER JOIN sess se ON se.id = sv.sess_id " +
				"INNER JOIN sub su ON su.id = se.sub_id " +
				"WHERE v.exp_id = " + expId + " AND su.is_sim = " + isSim + " AND se.is_ended = " + isSessEnded + " " +
				"ORDER BY se.id, v.id"
			);
			
			while (rsVar.next()) {
				res.append(
					"<tr>" +
					"<td>" + rsVar.getLong("sess_id")                          + "</td>" +
					"<td>" + $.date2str(rsVar.getTimestamp("sess_created"))    + "</td>" +
					"<td>" + (rsVar.getBoolean("sess_valid") ? "1" : "0")      + "</td>" +
					"<td>" + rsVar.getLong("sub_id")                           + "</td>" +
					"<td>" + rsVar.getLong("var_id")                           + "</td>" +
					"<td>" + rsVar.getString("var_name")                       + "</td>" +
					"<td>" + rsVar.getString("var_val")                        + "</td>" +
					"<td>" + $.date2str(rsVar.getTimestamp("var_val_created")) + "</td>" +
					"</tr>"
				);
			}
			
			rsVar.close();
			res.append("</table>");
			
			
			// (6) Design:
			// ...
			
			// (7) Finish:
			res.append(
				"<br />" +
				"</body>" +
				"</html>"
			);
		}
		finally {
			if (dbi != null) dbi.close();
		}
		
		return res;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static StringBuilder getRepFinal(long expId, boolean isSim, boolean isSessEnded, String varNameSep, String missVal)  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		StringBuilder res = new StringBuilder();
		DBI dbi = null;
		
		try {
			dbi = new DBI();
			
			if (varNameSep == null) varNameSep = VAR_NAME_SEP_DEF;
			if (missVal == null) missVal = ".";
			
			// (4) Trials:
			// (4.1) Variable names:
			int varCnt = 0;
			StringBuilder varNames = new StringBuilder();
			
			ResultSet rsTrialVarName = dbi.execQry(
				"SELECT " +
					"v.name AS var_name " +
				"FROM var v " +
				"WHERE v.exp_id = " + expId + " " +
				"ORDER BY v.id"
			);
			while (rsTrialVarName.next()) {
				varNames.append("var" + varNameSep + rsTrialVarName.getString("var_name") + (rsTrialVarName.isLast() ? "" : "\t"));
				varCnt++;
			}
			rsTrialVarName.close();
			
			// (4.2) Header row:
			res.append("trial" + varNameSep + "id\ttrial" + varNameSep + "created\tsess" + varNameSep + "id\tsess" + varNameSep + "created\tsess" + varNameSep + "valid\tsub" + varNameSep + "id\tcond" + varNameSep + "id\tcond" + varNameSep + "name\tblock" + varNameSep + "id\tblock" + varNameSep + "name\ttrial" + varNameSep + "idx" + varNameSep + "sess\ttrial" + varNameSep + "idx" + varNameSep + "block\tdec" + varNameSep + "ord\tdec" + varNameSep + "sub" + varNameSep + "t\tdec" + varNameSep + "sub\tdec" + varNameSep + "mod\tout" + varNameSep + "sub\tout" + varNameSep + "sub" + varNameSep + "tot" + varNameSep + "b\tout" + varNameSep + "sub" + varNameSep + "tot" + varNameSep + "a\tout" + varNameSep + "mod\tout" + varNameSep + "mod" + varNameSep + "tot" + varNameSep + "b\tout" + varNameSep + "mod" + varNameSep + "tot" + varNameSep + "a\tamt" + varNameSep + "hit" + varNameSep + "id\tamt" + varNameSep + "worker" + varNameSep + "id\tamt" + varNameSep + "assign" + varNameSep + "id" + (varCnt > 0 ? "\t" + varNames.toString() : "") + "\n");
			
			// (4.3) Table body:
			ResultSet rsTrial = dbi.execQry(
				"SELECT " +
					"t.id AS trial_id, " +
					"t.created AS trial_created, " +
					"se.id AS sess_id, " +
					"se.created AS sess_created, " +
					"se.is_valid AS sess_valid, " +
					"COALESCE(se.amt_hit_id, '" + missVal + "') AS amt_hit_id, " +
					"COALESCE(se.amt_assign_id, '" + missVal + "') AS amt_assign_id, " +
					"su.id AS sub_id, " +
					"COALESCE(su.amt_worker_id, '" + missVal + "') AS amt_worker_id, " +
					"c.id AS cond_id, " +
					"c.name AS cond_name, " +
					"t.block_id, " +
					"b.name AS block_name, " +
					"t.idx_sess, " +
					"t.idx_block, " +
					"t.dec_ord, " +
					"t.dec_sub_t, " +
					"COALESCE(CASE WHEN t.dec_sub = 0 THEN '0' ELSE t.dec_sub::text END, '" + missVal + "') AS dec_sub, " +
					"COALESCE(CASE WHEN t.dec_mod = 0 THEN '0' ELSE t.dec_mod::text END, '" + missVal + "') AS dec_mod, " +
					"COALESCE(CASE WHEN t.out_sub            = 0 THEN '0' ELSE trim(trailing '.' from trim(trailing '0' from t.out_sub::text))            END, '" + missVal + "') AS out_sub, "            +
					"COALESCE(CASE WHEN t.out_sub_tot_before = 0 THEN '0' ELSE trim(trailing '.' from trim(trailing '0' from t.out_sub_tot_before::text)) END, '" + missVal + "') AS out_sub_tot_before, " +
					"COALESCE(CASE WHEN t.out_sub_tot_after  = 0 THEN '0' ELSE trim(trailing '.' from trim(trailing '0' from t.out_sub_tot_after::text))  END, '" + missVal + "') AS out_sub_tot_after, "  +
					"COALESCE(CASE WHEN t.out_mod            = 0 THEN '0' ELSE trim(trailing '.' from trim(trailing '0' from t.out_mod::text))            END, '" + missVal + "') AS out_mod, "            +
					"COALESCE(CASE WHEN t.out_mod_tot_before = 0 THEN '0' ELSE trim(trailing '.' from trim(trailing '0' from t.out_mod_tot_before::text)) END, '" + missVal + "') AS out_mod_tot_before, " +
					"COALESCE(CASE WHEN t.out_mod_tot_after  = 0 THEN '0' ELSE trim(trailing '.' from trim(trailing '0' from t.out_mod_tot_after::text))  END, '" + missVal + "') AS out_mod_tot_after "   +
				"FROM trial t " +
				"INNER JOIN block b ON b.id = t.block_id " +
				"INNER JOIN cond c ON c.id = b.cond_id " +
				"INNER JOIN sess se ON se.id = t.sess_id " +
				"INNER JOIN sub su ON su.id = se.sub_id " +
				"WHERE c.exp_id = " + expId + " AND su.is_sim = " + isSim + " AND se.is_ended = " + isSessEnded + " " +
				"ORDER BY se.id, t.id"
			);
			
			while (rsTrial.next()) {
				long sessId = rsTrial.getLong("sess_id");
				
				// (4.3.1) Trial, session, subject, condition, and block info:
				res.append(
					rsTrial.getLong("trial_id")                       + "\t" +
					$.date2str(rsTrial.getTimestamp("trial_created")) + "\t" +
					sessId                                            + "\t" +
					$.date2str(rsTrial.getTimestamp("sess_created"))  + "\t" +
					(rsTrial.getBoolean("sess_valid") ? "1" : "0")    + "\t" +
					rsTrial.getLong("sub_id")                         + "\t" +
					rsTrial.getLong("cond_id")                        + "\t" +
					rsTrial.getString("cond_name")                    + "\t" +
					rsTrial.getLong("block_id")                       + "\t" +
					rsTrial.getString("block_name")                   + "\t" +
					rsTrial.getInt("idx_sess")                        + "\t" +
					rsTrial.getInt("idx_block")                       + "\t" +
					rsTrial.getString("dec_ord")                      + "\t" +
					rsTrial.getLong("dec_sub_t")                      + "\t" +
					rsTrial.getString("dec_sub")                      + "\t" +
					rsTrial.getString("dec_mod")                      + "\t" +
					rsTrial.getString("out_sub")                      + "\t" +
					rsTrial.getString("out_sub_tot_before")           + "\t" +
					rsTrial.getString("out_sub_tot_after")            + "\t" +
					rsTrial.getString("out_mod")                      + "\t" +
					rsTrial.getString("out_mod_tot_before")           + "\t" +
					rsTrial.getString("out_mod_tot_after")            + "\t" +
					rsTrial.getString("amt_hit_id")                   + "\t" +
					rsTrial.getString("amt_worker_id")                + "\t" +
					rsTrial.getString("amt_assign_id")
				);
				
				// (4.3.2) Variable values:
				ResultSet rsTrialVarVal = dbi.execQry(
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					"SELECT " +
						"v.id AS var_id, " +
						"sv.val AS var_val " +
					"FROM var v " +
					"LEFT JOIN sess_var sv ON sv.var_id = v.id " +
					"LEFT JOIN sess se ON se.id = sv.sess_id " +
					"LEFT JOIN sub su ON su.id = se.sub_id " +
					"WHERE v.exp_id = " + expId + " AND se.id = " + sessId + " AND su.is_sim = " + isSim + " AND se.is_ended = " + isSessEnded + " " +
					"ORDER BY v.id"
				);
				if (DBI.getRecCnt(rsTrialVarVal) == 0) {
					res.append("\t" + $.strRep("\t", varCnt - 1));
				}
				else while (rsTrialVarVal.next()) res.append("\t" + rsTrialVarVal.getString("var_val").replaceAll("\\t", " "));
				rsTrialVarVal.close();
				
				res.append("\n");
			}
			
			rsTrial.close();
		}
		finally {
			if (dbi != null) dbi.close();
		}
		
		return res;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static StringBuilder getRepAmt(long expId, boolean isProd)  throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		StringBuilder res = new StringBuilder();
		DBI dbi = null;
		
		try {
			dbi = new DBI();
			
			// (0) Init:
			res.append(
				"<!DOCTYPE html>" +
				"<html>" +
				"<head>" +
				"<link rel=\"stylesheet\" type=\"text/css\" href=\"style/exp-rep-amt.css\" />" +
				"<script type=\"text/javascript\" src=\"script/$.js\"></script>" +
				//"<script type=\"text/javascript\" src=\"script/amt.js\"></script>" +
				"</head>" +
				"<body>"
			);
			
			// (1) Report and experiment:
			ResultSet rsExp = dbi.execQry("SELECT name, amt_pay_base FROM exp WHERE id = " + expId);
			rsExp.next();
			res.append(
				"<table cellpadding=\"0\" cellspacing=\"0\" class=\"v\">" +
				"<tr><th>Report date</th><td>:</td><td>" + $.date2str(new Date()) + "</td></tr>" +
				"<tr><th>Experiment name</th><td>:</td><td>" + rsExp.getString("name") + "</td></tr>" +
				"<tr><th>Experiment ID</th><td>:</td><td>" + expId + "</td></tr>" +
				"</table>"
			);
			rsExp.close();
			
			// (2) Subjects:
			res.append(
				"<br />" +
				"<table cellpadding=\"0\" cellspacing=\"0\" class=\"h\">" +
				"<tr><th>sess-id</th><th>worker-id</th><th>action</th><th>bonus</th></tr>"
			);
			
			ResultSet rsSub = dbi.execQry(
				"SELECT " +
					"su.id AS sub_id, " +
					"su.created AS sub_created, " +
					"COALESCE(su.amt_worker_id,'') AS amt_worker_id, " +
					"amt_pay_base, " +
					"amt_pay_bonus, " +
					"amt_pay_base_ts, " +
					"amt_pay_bonus_ts, " +
					"se.id AS sess_id, " +
					"se.created AS sess_created, " +
					"se.is_valid AS sess_valid, " +
					"COALESCE(se.amt_hit_id,'') AS amt_hit_id, " +
					"COALESCE(se.amt_assign_id,'') AS amt_assign_id, " +
					"c.amt_pay_bonus_mult, " +
					"c.amt_pay_bonus_max " +
				"FROM sub su " +
				"INNER JOIN sess se ON se.sub_id = su.id " +
				"INNER JOIN cond c ON c.id = se.cond_id " +
				"WHERE su.exp_id = " + expId + " AND se.is_ended = true AND amt_worker_id IS NOT NULL " +
				"ORDER BY su.id"
			);
			
			while (rsSub.next()) {
				long sessId = rsSub.getLong("sess_id");
				boolean isSessValid = rsSub.getBoolean("sess_valid");
				
				Long outSubTotAfter = DBI.getLong(dbi, "SELECT out_sub_tot_after FROM trial WHERE sess_id = " + sessId + " ORDER BY id DESC LIMIT 1");
				int amtPayBonusCond    = (outSubTotAfter == null ? 0 : (int) Math.round(outSubTotAfter.doubleValue() * rsSub.getDouble("amt_pay_bonus_mult")));
				
				res.append(
					"<tr>" +
					"<td>" + sessId                           + "</td>" +
					"<td>" + rsSub.getString("amt_worker_id") + "</td>" +
					"<td>" +
						(isSessValid
							? "<span class=\"act-accept\">accept</span>"
							: "<span class=\"act-reject\">reject</span>"
						) +
					"</td>" +
					"<td>" +
						(isSessValid
							? $.cents2dollars(amtPayBonusCond)
							: "-"
						) +
					"</td>" +
					"</tr>"
				);
			}
			
			rsSub.close();
			
			// (3) Finish:
			res.append(
				"</table>" +
				"</body>" +
				"</html>"
			);
		}
		finally {
			if (dbi != null) dbi.close();
		}
		
		return res;
	}
}
