package main;

import util.$;


/**
 * TODO: Consider renaming this class to 'Block' (eventually).
 * 
 * @author Tomek D. Loboda
 */
public class GameIBL {
	private int timeMax;  // maximum discrete time index (e.g., the trial number)
	private int time;     // current discrete time index (e.g., the trial number)
	
	private float[][] payoffA;
	private float[][] payoffB;
	
	private Model model = null;
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * An example of how decision counts and payoff matrices matrices are used to build the actual matrices.
	 * 
	 *   decisionCntA: 2
	 *   decisionCntB: 3
	 *   
	 *   specsPayoffA: 0 1 2 3 4 5
	 *   specsPayoffA: A0-B0 A0-B1 A0-B2 A1-B0 A1-B1 A1-B2
	 *   
	 *   payoffA:
	 *      B0 B1 B2
	 *   A0  0  1  2
	 *   A1  3  4  5
	 */
	public Result start(int timeMax, int decisionCntA, int decisionCntB, String specsPayoffA, String specsPayoffB, float modNoise, float modDecay) {
		// (1) Decode and validate arguments:
		this.timeMax = timeMax;
		this.time    = -1;
		
		// (1.1) Payoff matrices:
		String A[] = specsPayoffA.split(",");
		String B[] = specsPayoffB.split(",");
		
		
		if (A.length != (decisionCntA * decisionCntB) || B.length != (decisionCntA * decisionCntB)) return new Result(false, "msg:\"The number of decisions and the dimensions of the payoff matrices have to match.\"");
		
		payoffA = new float[decisionCntA][decisionCntB];
		payoffB = new float[decisionCntA][decisionCntB];
		
		for (int i = 0; i < decisionCntA; i++) {
			for (int j = 0; j < decisionCntB; j++) {
				payoffA[i][j] = $.str2float(A[i*decisionCntB + j], null, null);
				payoffB[i][j] = $.str2float(B[i*decisionCntB + j], null, null);
				
				//System.out.println("payoffA[" + i + "][" + j +"]: " + payoffA[i][j]);
			}
		}
		
		// (2) Initialize:
		model = new Model(decisionCntB, modNoise, modDecay, false);
		
		return new Result(true, "");
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public Result step(int decisionA) {
		if (time >= timeMax) return new Result(false, "msg:\"The game has ended.\"");
		time++;
		
		int decisionB = model.makeDecision(time);
		
		float outcomeA = payoffA[decisionA][decisionB];
		float outcomeB = payoffB[decisionA][decisionB];
		
		model.rememberInstance(time, decisionB, outcomeB);
		
		return new Result(true, "time:" + time + ",isGameOver:" + isOver() + ",decisionA:" + decisionA + ",decisionB:" + decisionB + ",outcomeA:" + outcomeA + ",outcomeB:" + outcomeB);
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public boolean isOver() { return (time == timeMax); }
}
