package main;

import inter.DBI;

import java.sql.ResultSet;
import java.sql.SQLException;


/**
 * A probabilistic model.
 * 
 * @author Tomek D. Loboda
 */
public class BlockModelProb extends BlockModel {
	private final double pA0, pB0, payoffA0, payoffA1, payoffB0, payoffB1;
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public BlockModelProb(long modelId, int trialCnt, String decisionOrd)  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		super(modelId, trialCnt, decisionOrd);
		
		DBI dbi = null;
		try {
			dbi = new DBI();
			
			ResultSet rs = dbi.execQry("SELECT name, p_a_0, p_b_0, payoff_a_0, payoff_a_1, payoff_b_0, payoff_b_1 FROM model_prob WHERE id = " + modelId);
			rs.next();
			
			pA0 = rs.getDouble("p_a_0");
			pB0 = rs.getDouble("p_b_0");
			
			payoffA0 = rs.getDouble("payoff_a_0");
			payoffA1 = rs.getDouble("payoff_a_1");
			payoffB0 = rs.getDouble("payoff_b_0");
			payoffB1 = rs.getDouble("payoff_b_1");
			
			rs.close();
		}
		finally {
			if (dbi != null) dbi.close();
		}
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Returns an outcome of a random draw given the probabilities of the two outcomes associated with the decision.
	 */
	@Override
	public double getOutcome(int decisionIdx, long decisionT, long sessId, long blockId, int trialIdxSess, int trialIdxBlock, double outcomeTotal, boolean doUpdOut)  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		DBI dbi = null;
		try {
			dbi = new DBI();
			
			lastDecisionIdx = decisionIdx;
			int decisionName = lstDecisions.get(decisionIdx).getName();
			
			double r = Math.random();
			
			if (decisionName == 0) lastOutcome = (r <= pA0 ? payoffA0: payoffA1);
			else                   lastOutcome = (r <= pB0 ? payoffB0: payoffB1);
			
			dbi.execUpd("INSERT INTO trial (sess_id, block_id, idx_sess, idx_block, dec_ord, dec_sub_t, dec_sub, out_sub, out_sub_tot_before, out_sub_tot_after) VALUES (" + sessId + "," + blockId + "," + trialIdxSess + "," + trialIdxBlock + ",'" + decisionOrd + "'," + decisionT + "," + decisionName + "," + lastOutcome + "," + outcomeTotal + "," + (doUpdOut ? outcomeTotal + lastOutcome : outcomeTotal) + ")");
		}
		finally {
			if (dbi != null) dbi.close();
		}
		
		return lastOutcome;
	}
}
