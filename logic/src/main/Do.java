/**
 * List of server session attributes:
 *   sess         : the session (Sess) object created for each subject
 *   sim-sess-lst : the map storing all sessions of simulated subjects (key: session ID)
 *
 * Miscellaneous:
 *   1x2 game from Lejarraga et. al (2010)
 *     http://localhost:8080/dgames/do?cmd=game-start&trial-max=10&dec-cnt-a=2&dec-cnt-b=2&payoff-a=3.2,3,3.2,3&payoff-b=3.2,3,3.2,3&mod-noise=1.5&mod-decay=5
 *   Prisoner's Dillema:
 *     http://localhost:8080/dgames/do?cmd=game-start&trial-max=10&dec-cnt-a=2&dec-cnt-b=2&payoff-a=5,10,-10,2&payoff-b=5,-10,10,2&mod-noise=1.5&mod-decay=5
 *     http://localhost:8080/dgames/do?cmd=game-step&dec-a=0
 *   Model validation:
 *     http://localhost:8080/dgames/do?cmd=validate-1x2&filename=1xN-lejarreja-2010.csv&trial-max=10&mod-noise=1.5&mod-decay=5
 *     http://localhost:8080/dgames/do?cmd=validate-1x2&filename=1xN-TPT.csv&trial-max=100&mod-noise=1.5&mod-decay=5
 */

package main;

import inter.DBI;
import inter.HTTPI;
import inter.JSI;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;

import javax.naming.InitialContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.apache.http.ParseException;
import org.xml.sax.SAXException;

import exception.LogicException;
import exception.ParamException;


/**
 * @author Tomek D. Loboda
 */
@SuppressWarnings("serial")
@WebServlet(name="DGames", urlPatterns={"/do"})
public class Do extends HttpServlet {
	public static final String APP_NAME = "DGames";

	public static final String URL_SERVER = "...";

	public static final String APP_ERR_SESS_NOT_ENDED = APP_NAME + " (error): Subject session could not be ended properly; sessId = ";
	public static final String APP_ERR_AMT_ERR        = APP_NAME + " (error): AMT operation failed";

	public static final String RES_ERR_SUB_SESS_NOT_FOUND = "Subject session not found. It could have not been properly initiated in the first place or deleted before the current request has been received.";
	public static final String RES_ERR_INCORRECT_PASSWD   = "Incorrect password provided.";

	private static String pathApp;
	private static String pathWork;

	public static DataSource datasrc;

	public static Hashtable<String, String> delim = new Hashtable<String, String>();


	// -----------------------------------------------------------------------------------------------------------------
	private void dispatch(HttpServletRequest req, HttpServletResponse res)  throws Exception {
		ReqCtx rc = new ReqCtx(req);
		String cmd = rc.getStr("cmd", true);
		execCmd(req, res, rc, cmd);
	}


	// -----------------------------------------------------------------------------------------------------------------
	public void doGet(HttpServletRequest req, HttpServletResponse resp)  throws IOException, ServletException { doPost(req, resp); }


	// -----------------------------------------------------------------------------------------------------------------
	public void doPost(HttpServletRequest req, HttpServletResponse res)  throws ServletException, IOException {
		try { dispatch(req, res); }
		catch (Exception e) {
			if ((e instanceof ParamException) || (e instanceof LogicException)) {
				HTTPI.retTxt(res, "{outcome:false,msg:" + JSI.str2js(e.getMessage()) + "}");
			}
			else HTTPI.retStr(res, e);
		}
		catch (java.lang.OutOfMemoryError e) {
			HTTPI.retTxt(res, "{outcome:false,msg:" + JSI.str2js(e.getMessage()) + "}");
		}
	}


	// -----------------------------------------------------------------------------------------------------------------
	public static String getPathApp()  { return pathApp;  }
	public static String getPathWork() { return pathWork; }


	// -----------------------------------------------------------------------------------------------------------------
	@Override
	public void init() {
		try {
			InitialContext cxt = new InitialContext();
			datasrc = (DataSource) cxt.lookup("java:/comp/env/jdbc/postgres_local_dgames");

			// Delimiters:
			delim.put("tab", "\t");
			delim.put("new-line", "\n");

			// Paths:
			pathApp  = this.getServletContext().getRealPath("") + File.separatorChar;
			pathWork = pathApp + "work" + File.separatorChar;

			sysHealthCheck();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	// -----------------------------------------------------------------------------------------------------------------
	@Override
	public void destroy() {
		sysHealthCheck();
	}


	// -----------------------------------------------------------------------------------------------------------------
	@SuppressWarnings("unchecked")
	private void execCmd(HttpServletRequest req, HttpServletResponse res, ReqCtx rc, String cmd)  throws Exception {
		HttpSession sessServ = req.getSession();

		// 1x1 game (model validation):
		if (cmd.equals("validate-1x2")) {
			Model m = new Model(2, rc.getFloat("mod-noise", true, 0f, 50f), rc.getFloat("mod-decay", true, 0f, 50f), true);
			HTTPI.retTxt(res, m.playSet1x2(pathApp + rc.getStr("filename", true), ",", rc.getInt("trial-max", true, 0, 200)));
		}


		// NxN game (against human):
		else if (cmd.equals("game-start")) {
			Result r = null;
			GameIBL g = (GameIBL) sessServ.getAttribute("game");

			if (g == null) g = new GameIBL();
			r = g.start(
				rc.getInt("trial-max",   true, 0, 200),
				rc.getInt("dec-cnt-a",   true, 0, 4),
				rc.getInt("dec-cnt-b",   true, 0, 4),
				rc.getStr("payoff-a",    true),
				rc.getStr("payoff-b",    true),
				rc.getFloat("mod-noise", true, 0f, 50f),
				rc.getFloat("mod-decay", true, 0f, 50f)
			);

			if (!r.isOk()) {
				HTTPI.retRes(res, r);
				return;
			}

			sessServ.setAttribute("game", g);

			HTTPI.retRes(res, new Result(true, ""));
		}

		else if (cmd.equals("game-step")) {
			GameIBL g = (GameIBL) sessServ.getAttribute("game");
			HTTPI.retRes(res, (g == null ? new Result(false, "msg:\"The game has not started.\"") : g.step(rc.getInt("dec-a", true, 0, 4))));
		}


		// Admin:
		else if (cmd.equals("admin-del-sim-sub")) {
			endAllSimSubSess(sessServ);
			HTTPI.retRes(res, Admin.delSubSim(rc.getLong("exp-id", true, 0l, null)));
		}

		else if (cmd.equals("admin-sys-reset")) {
			if (!Admin.isPasswdOk(rc.getStr("passwd", true))) {
				HTTPI.retRes(res, new Result(false, "msg:\"" + RES_ERR_INCORRECT_PASSWD + "\""));
			}
			else {
				endAllSimSubSess(sessServ);
				HTTPI.retRes(res, Admin.sysReset());
			}
		}


		// Attr:
		else if (cmd.equals("attr-set")) {
			HTTPI.retRes(res, DBI.attrSet(rc.getStr("obj-name", true), rc.getLong("obj-id", true, 0l, null), rc.getStr("attr-name", true), rc.getStr("attr-val", true)));
		}


		// AMT:
		// ...


		// Experiment:
		else if (cmd.equals("exp-get-amt-lst")) {
			HTTPI.retHTML(res, Exp.getRepAmt(rc.getLong("exp-id", true, 0l, null), rc.getBool("is-prod", true)));
		}

		else if (cmd.equals("exp-get-inf")) {
			HTTPI.retRes(res, Exp.getInf(rc.getLong("exp-id", true, 0l, null), rc.getBool("is-sim", true)));
		}

		else if (cmd.equals("exp-get-lst")) {
			HTTPI.retRes(res, Exp.getLst());
		}

		else if (cmd.equals("exp-get-rep")) {
			HTTPI.retHTML(res, Exp.getRep(rc.getLong("exp-id", true, 0l, null), rc.getBool("is-sim", true), rc.getBool("is-sess-ended", true), rc.getStr("var-name-sep", false), rc.getStr("miss-val", false)));
		}

		else if (cmd.equals("exp-get-rep-final")) {
			HTTPI.retTxt(res, Exp.getRepFinal(rc.getLong("exp-id", true, 0l, null), rc.getBool("is-sim", true), rc.getBool("is-sess-ended", true), rc.getStr("var-name-sep", false), rc.getStr("miss-val", false)));
		}


		// Session:
		else if (cmd.equals("sess-end")) {
			Sess sessSub = (Sess) sessServ.getAttribute("sess");
			if (sessSub == null) HTTPI.retRes(res, new Result(true, "msg:\"Subject session not found. Nothing to do.\""));
			else {
				sessSub.end();
				sessServ.invalidate();
				HTTPI.retRes(res, new Result(true, ""));
			}
		}

		else if (cmd.equals("sess-end-sim")) {
			ConcurrentHashMap<Long,Sess> sessSubLst = (ConcurrentHashMap<Long,Sess>) sessServ.getAttribute("sim-sess-lst");

			if (sessSubLst == null) HTTPI.retRes(res, new Result(true, "msg:\"No simulated subject sessions found. Nothing to do.\""));
			else {
				long subId = rc.getLong("sess-id", true, 0l, null);
				Sess s = sessSubLst.get(subId);

				if (s == null) HTTPI.retRes(res, new Result(true, "msg:\"Subject session not found. Nothing to do.\""));
				else {
					s.end();
					sessSubLst.remove(subId);
					HTTPI.retRes(res, new Result(true, ""));
				}
			}
		}

		else if (cmd.equals("sess-end-all-sim")) {
			endAllSimSubSess(sessServ);
		}

		else if (cmd.equals("sess-exec-action")) {
			Sess sessSub = (Sess) sessServ.getAttribute("sess");

			if (sessSub == null) HTTPI.retRes(res, new Result(false, "msg:\"" + RES_ERR_SUB_SESS_NOT_FOUND + "\""));
			else                 HTTPI.retRes(res, sessSub.execAction(rc));
		}

		else if (cmd.equals("sess-exec-action-sim")) {
			ConcurrentHashMap<Long,Sess> sessSubLst = (ConcurrentHashMap<Long,Sess>) sessServ.getAttribute("sim-sess-lst");

			Sess sessSub = null;
			if (sessSubLst != null) sessSub = sessSubLst.get(rc.getLong("sess-id", true, 0l, null));

			if (sessSubLst == null || sessSub == null) HTTPI.retRes(res, new Result(false, "msg:\"" + RES_ERR_SUB_SESS_NOT_FOUND + "\""));
			else                                       HTTPI.retRes(res, sessSub.execAction(rc));
		}

		else if (cmd.equals("sess-get-state")) {
			Sess sessSub = (Sess) sessServ.getAttribute("sess");
			if (sessSub == null) {
				sessSub = new Sess(rc.getLong("exp-id", true, 0l, null), rc.getStr("amt-worker-id", false), rc.getStr("amt-assign-id", false), false);
				sessServ.setMaxInactiveInterval(Cond.getSessTimeout(sessSub.getCondId()));
				sessServ.setAttribute("sess", sessSub);
			}

			HTTPI.retRes(res, sessSub.getState());
		}

		else if (cmd.equals("sess-get-state-sim")) {
			ConcurrentHashMap<Long,Sess> sessSubLst = (ConcurrentHashMap<Long,Sess>) sessServ.getAttribute("sim-sess-lst");
			if (sessSubLst == null) {
				sessSubLst = new ConcurrentHashMap<Long,Sess>(8, 0.9f, 1);  // http://ria101.wordpress.com/2011/12/12/concurrenthashmap-avoid-a-common-misuse
				sessServ.setAttribute("sim-sess-lst", sessSubLst);
			}

			Long sessId = rc.getLong("sess-id", false, 0l, null);

			Sess sessSub = null;
			if (sessId != null) sessSub = sessSubLst.get(sessId);
			if (sessSub == null) {
				sessSub = new Sess(rc.getLong("exp-id", true, 0l, null), rc.getStr("amt-worker-id", false), rc.getStr("amt-assign-id", false), true);
				sessId = sessSub.getSessId();

				sessSubLst.put(sessId, sessSub);
			}

			HTTPI.retRes(res, sessSub.getState());
		}

		else if (cmd.equals("sess-get-lst-sim")) {
			ConcurrentHashMap<Long,Sess> sessSubLst = (ConcurrentHashMap<Long,Sess>) sessServ.getAttribute("sim-sess-lst");

			if (sessSubLst == null) HTTPI.retRes(res, new Result(true, "sess:[]"));
			else {
				long expId = rc.getLong("exp-id", true, 0l, null);
				synchronized (sessSubLst) {
					StringBuilder sb = new StringBuilder("sess:[");
					Iterator<Long> it = sessSubLst.keySet().iterator();
					while (it.hasNext()) {
						Long sessId = it.next();
						Sess sessSub = sessSubLst.get(sessId);
						if (sessSub.hasEnded()) sessSub.end();
						else {
							if (sessSub.getExpId() == expId) sb.append("{" + sessSub.getStatusStr() + "}" + (it.hasNext() ? "," : ""));
						}
					}
					sb.append("]");
					HTTPI.retRes(res, new Result(true, sb));
				}
			}
		}


		// Miscellaneous:
		else if (cmd.equals("ping")) { HTTPI.retTxt(res, "pong"); }


		// Unknown command:
		else { HTTPI.retRes(res, new Result(false, "msg:\"Unknown command '" + cmd + "'.\"")); }
	}


	// -----------------------------------------------------------------------------------------------------------------
	@SuppressWarnings("unchecked")
	public static Result endAllSimSubSess(HttpSession sessServ)  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException, KeyManagementException, UnrecoverableKeyException, KeyStoreException, NoSuchAlgorithmException, CertificateException, SignatureException, UnsupportedEncodingException, ParseException, XPathExpressionException, IOException, ParserConfigurationException, SAXException {
		ConcurrentHashMap<Long,Sess> sessSubLst = (ConcurrentHashMap<Long,Sess>) sessServ.getAttribute("sim-sess-lst");

		if (sessSubLst == null) new Result(true, "msg:\"No simulated subject sessions found. Nothing to do.\"");
		else {
			synchronized (sessSubLst) {
				Iterator<Sess> it = sessSubLst.values().iterator();
				while (it.hasNext()) it.next().end();
				sessSubLst.clear();
			}
			new Result(true, "");
		}
		return new Result(true, "");
	}


	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Performs the system health check. This method is called both on startup and shutdown to ensure that the
	 * database is in a logically consistent state.
	 */
	public void sysHealthCheck() {
		DBI dbiX = null;
		Statement stX = null;

		try {
			dbiX = new DBI();
			stX = dbiX.transBegin();
			stX.executeUpdate("UPDATE sess SET is_valid = false WHERE is_ended = false");
			stX.executeUpdate("UPDATE sess SET is_ended = true  WHERE is_ended = false");
			dbiX.transCommit();
		}
		catch (SQLException e) {
			try { dbiX.transRollback(); } catch (SQLException e1) {}
		}
		catch (InstantiationException e) {}
		catch (IllegalAccessException e) {}
		catch (ClassNotFoundException e) {}
		finally {
			try {
				if (dbiX != null) dbiX.close();
			}
			catch (SQLException e) {}
		}
	}
}
