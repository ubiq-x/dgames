package main;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


/**
 * Stores information about all outcomes associated with one of the decision options (e.g., 'defect' in the Prisoner's 
 * Dilemma game). All other decision options should be stored in a higher level object.
 * 
 * @author Tomek D. Loboda
 */
public class IBLDecision {
	private final Model model;
	
	private final double tau;  // model noise * sqrt(2)
	
	private final Map<Float,Outcome> mapOutcome = new HashMap<Float,Outcome>();
		// Maps the outcome (or the value) observed after making a decision to the Outcome object which is used to 
		// remember all occurrences of that outcome and to calculate activation.
	
	private final Map<Integer,Double> mapBlendedValue;   // (debug)
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public IBLDecision(Model model) {
		this.model = model;
		
		tau = model.getNoise() * Math.sqrt(2);
		
		if (model.isDebug()) mapBlendedValue  = new HashMap<Integer,Double>();
		else mapBlendedValue  = null;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public Map<Integer,Double> getBlendedValues()  { return mapBlendedValue;  }
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public Map<Float,Outcome> getOutcomes() { return mapOutcome; }
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Calculates the blended value (V; Lebiere, 1999).
	 */
	public double getBlendedValue(int trial) {
		double V = 0;
		Iterator<Float> it = mapOutcome.keySet().iterator();
		while (it.hasNext()) {
			float outcome = it.next();
			//System.out.println("    Outcome: " + outcome);
			V += outcome * getRetrievalProb(outcome, trial);
			//System.out.println("    P: " + a);
		}
		
		if (model.isDebug()) mapBlendedValue.put(trial, V);
		
		return V;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Calculates the retrieval probability (p).
	 */
	public double getRetrievalProb(float outcome_i, int trial) {
		double a = 0;
		double b = 0;
		Iterator<Float> it = mapOutcome.keySet().iterator();
		while (it.hasNext()) {
			float outcome_j = it.next();
			//System.out.println("      Outcomes: " + outcome_i + "," + outcome_j);
			double tmp = Math.exp(mapOutcome.get(outcome_j).getActivation(trial) / tau);
			
			if (outcome_j == outcome_i) a = tmp;
			b += tmp;
			//System.out.println("      P: " + tmp);
		}
		
		double p = a / b;
		if (model.isDebug()) mapOutcome.get(outcome_i).setRetrievalProb(trial, p);
		
		return p;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Should be called whenever an outcome is observed.
	 */
	public void remember(float outcome, int trial) {
		if (!mapOutcome.containsKey(outcome)) {
			mapOutcome.put(outcome, new Outcome(model));
		}
		
		mapOutcome.get(outcome).remember(trial);
	}
}


/*
function [RetrivalProb1, RetrivalProb2, RetrivalProb3] = CalculateRetrivalProb(Instance1, Instance2, Instance3, s) 

Retrival1 = exp(Instance1.ACTIV/(s*sqrt(2)));
Retrival2 = exp(Instance2.ACTIV/(s*sqrt(2)));
Retrival3 = exp(Instance3.ACTIV/(s*sqrt(2)));

Retrival = Retrival1 + Retrival2 + Retrival3;

RetrivalProb1 = Retrival1/Retrival;
RetrivalProb2 = Retrival2/Retrival;
RetrivalProb3 = Retrival3/Retrival;
*/

/*
function BlendedValue = CalcBlendedVal (instance1, instance2, instance3)
BlendedValue = (instance1.OUTCOME * instance1.RETRIVALPROB) + (instance2.OUTCOME * instance2.RETRIVALPROB) + (instance3.OUTCOME * instance3.RETRIVALPROB);
*/
