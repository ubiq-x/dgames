package main;

import inter.DBI;

import java.sql.SQLException;


/**
 * @author Tomek D. Loboda
 */
public class Admin {
	static final String PASSWD = "foraiur";
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Deletes simulated subjects associated with the specified experiment.
	 */
	public static Result delSubSim(long expId)  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		DBI dbi = null;
		try {
			dbi = new DBI();
			dbi.execUpd(
				"DELETE FROM sub " +
				"USING sess, cond " +
				"WHERE sub.id = sess.sub_id AND cond.id = sess.cond_id AND cond.exp_id = " + expId + " AND sub.is_sim = true"
			);
		}
		finally {
			if (dbi != null) dbi.close();
		}
		
		return new Result(true, "");
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static boolean isPasswdOk(String passwd) { return passwd.equals(PASSWD); }
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static Result sysReset()  throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		DBI dbi = null;
		try {
			dbi = new DBI();
			dbi.execUpd("DELETE FROM sub");
			dbi.execUpd("DELETE FROM exp");
		}
		finally {
			if (dbi != null) dbi.close();
		}
		
		return new Result(true, "");
	}
}
