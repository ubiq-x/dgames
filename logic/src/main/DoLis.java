package main;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.sql.SQLException;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.apache.http.ParseException;
import org.xml.sax.SAXException;


/**
 * Session 
 * @author Tomek D. Loboda
 */
@WebListener
public class DoLis implements HttpSessionListener {
	
	// -----------------------------------------------------------------------------------------------------------------
	@Override
	public void sessionCreated(HttpSessionEvent se) {}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	@Override
	public void sessionDestroyed(HttpSessionEvent se) {
		HttpSession sessServ = se.getSession();
		
		Sess sessSub = (Sess) sessServ.getAttribute("sess");
		
		if (sessSub != null) {
			try {
				try {
					sessSub.end();
				}
				catch (KeyManagementException e) {
					e.printStackTrace();
				}
				catch (UnrecoverableKeyException e) {
					e.printStackTrace();
				}
				catch (KeyStoreException e) {
					e.printStackTrace();
				}
				catch (NoSuchAlgorithmException e) {
					e.printStackTrace();
				}
				catch (CertificateException e) {
					e.printStackTrace();
				}
				catch (SignatureException e) {
					e.printStackTrace();
				}
				catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
				catch (ParseException e) {
					e.printStackTrace();
				}
				catch (XPathExpressionException e) {
					e.printStackTrace();
				}
				catch (IOException e) {
					e.printStackTrace();
				}
				catch (ParserConfigurationException e) {
					e.printStackTrace();
				}
				catch (SAXException e) {
					e.printStackTrace();
				}
			}
			catch (SQLException e) {
				System.out.println(Do.APP_ERR_SESS_NOT_ENDED + sessSub.getSessId());
			}
			catch (InstantiationException e) {
				e.printStackTrace();
			}
			catch (IllegalAccessException e) {
				e.printStackTrace();
			}
			catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
		else {
			try {
				try {
					Do.endAllSimSubSess(sessServ);
				}
				catch (KeyManagementException e) {
					e.printStackTrace();
				}
				catch (UnrecoverableKeyException e) {
					e.printStackTrace();
				}
				catch (KeyStoreException e) {
					e.printStackTrace();
				}
				catch (NoSuchAlgorithmException e) {
					e.printStackTrace();
				}
				catch (CertificateException e) {
					e.printStackTrace();
				}
				catch (SignatureException e) {
					e.printStackTrace();
				}
				catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
				catch (ParseException e) {
					e.printStackTrace();
				}
				catch (XPathExpressionException e) {
					e.printStackTrace();
				}
				catch (IOException e) {
					e.printStackTrace();
				}
				catch (ParserConfigurationException e) {
					e.printStackTrace();
				}
				catch (SAXException e) {
					e.printStackTrace();
				}
			}
			catch (SQLException e) {
				System.out.println(Do.APP_ERR_SESS_NOT_ENDED + "-");
			}
			catch (InstantiationException e) {
				e.printStackTrace();
			}
			catch (IllegalAccessException e) {
				e.printStackTrace();
			}
			catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
	}
}
