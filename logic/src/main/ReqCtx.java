package main;

import inter.HTTPI;

import java.io.UnsupportedEncodingException;

import javax.servlet.http.HttpServletRequest;

import util.$;
import exception.ParamException;


/**
 * @author Tomek D. Loboda
 */
public class ReqCtx {
	private final HttpServletRequest req;
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * C'tor.
	 */
	public ReqCtx(HttpServletRequest req) {
		this.req = req;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public boolean getBool(String name, boolean required)  throws UnsupportedEncodingException, ParamException {
		String s = HTTPI.getParamStr(req, name);
		if (required && (s == null || s.length() == 0)) throw new ParamException("'" + name + "' parameter cannot be empty");
		return (s == null || !s.equals("1") ? false : true);
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public Float getFloat(String name, boolean required, Float min, Float max)  throws UnsupportedEncodingException, ParamException {
		String s = HTTPI.getParamStr(req, name);
		if (required && (s == null || s.length() == 0)) throw new ParamException("'" + name + "' parameter cannot be empty");
		if (s == null || s.length() == 0) return null;
		
		Float x = $.str2float(s, min, max);
		if (required && (x == null)) throw new ParamException("'" + name + "' parameter cannot be empty");
		
		if (min != null && x < min) throw new ParamException("'" + name + "' parameter cannot be smaller than " + min);
		if (max != null && x > max) throw new ParamException("'" + name + "' parameter cannot be greater than " + max);
		
		return x;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public Integer getInt(String name, boolean required, Integer min, Integer max)  throws UnsupportedEncodingException, ParamException {
		String s = HTTPI.getParamStr(req, name);
		if (required && (s == null || s.length() == 0)) throw new ParamException("'" + name + "' parameter cannot be empty");
		if (s == null || s.length() == 0) return null;
		
		Integer x = $.str2int(s, min, max);
		if (required && (x == null)) throw new ParamException("'" + name + "' parameter cannot be empty");
		
		if (min != null && x < min) throw new ParamException("'" + name + "' parameter cannot be smaller than " + min);
		if (max != null && x > max) throw new ParamException("'" + name + "' parameter cannot be greater than " + max);
		
		return x;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public Long getLong(String name, boolean required, Long min, Long max)  throws UnsupportedEncodingException, ParamException {
		String s = HTTPI.getParamStr(req, name);
		if (required && (s == null || s.length() == 0)) throw new ParamException("'" + name + "' parameter cannot be empty");
		if (s == null || s.length() == 0) return null;
		
		Long x = $.str2long(s, min, max);
		if (required && (x == null)) throw new ParamException("'" + name + "' parameter cannot be empty");
		
		if (min != null && x < min) throw new ParamException("'" + name + "' parameter cannot be smaller than " + min);
		if (max != null && x > max) throw new ParamException("'" + name + "' parameter cannot be greater than " + max);
		
		return x;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public String getStr(String name, boolean required)  throws UnsupportedEncodingException, ParamException {
		String s = HTTPI.getParamStr(req, name);
		if (required && (s == null || s.length() == 0)) throw new ParamException("'" + name + "' parameter cannot be empty");
		return s;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public String getStr(String name, String defVal)  throws UnsupportedEncodingException, ParamException {
		String s = HTTPI.getParamStr(req, name, defVal);
		return s;
	}
}
