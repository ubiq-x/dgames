package main;

import java.sql.SQLException;

import inter.DBI;


/**
 * @author Tomek D. Loboda
 */
public class Cond {
	
	// -----------------------------------------------------------------------------------------------------------------
	public static String getQry_lst(long expId, boolean isSim) {
		return
			"SELECT c.id, c.name, c.idx, c.out_init, c.amt_pay_bonus_max, c.amt_pay_bonus_mult, c.sub_cnt_max, COUNT(a.*) AS sub_cnt_curr " +
			"FROM cond c " +
			"LEFT JOIN (" +
				"SELECT se.cond_id " +
				"FROM sess se " +
				"INNER JOIN sub su ON su.id = se.sub_id " +
				"INNER JOIN cond c ON c.id = se.cond_id " +
				"WHERE ((se.is_ended = true AND se.is_valid = true) OR se.is_ended = false) AND su.is_sim = " + (isSim ? "true" : "false") + " AND c.exp_id = " + expId +
			") AS a ON a.cond_id = c.id " +
			"WHERE c.exp_id = " + expId + " " +
			"GROUP BY c.id, c.name, c.idx, c.out_init, c.amt_pay_bonus_max, c.amt_pay_bonus_mult, c.sub_cnt_max " +
			"ORDER BY c.idx";
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static int getSessTimeout(long condId)  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		return DBI.getLong((DBI) null, "SELECT timeout FROM cond WHERE id = " + condId).intValue();
	}
}
