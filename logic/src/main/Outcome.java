package main;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Stores information about a single outcome associated with one particular decision option (e.g., 'defect' in the 
 * Prisoner's Dilemma game). All other observed (and unobserved, if a need be) outcomes associated with that decision 
 * option should be stored in a higher level object.
 * 
 * TODO: Optimize the component B in getActivation() method by pre-computing all values for time differences of 1 to 
 *       100 or 200. If the difference is greater than that, compute it as needed.
 * 
 * @author Tomek D. Loboda
 */
public class Outcome {
	private final Model model;
	
	private final List<Integer> lstTrial = new ArrayList<Integer>();  // trials at which the outcome has been observed
	
	private final Map<Integer,Double> mapGamma = new HashMap<Integer,Double>();
		// Stores values of the gamma parameter generated at every trial at which an activation was calculated. This 
		// ensures that if an activation is calculated multiple times for a given trial, the same gamma value will be  
		// used every time.

	private final Map<Integer,Double> mapActivation;     // (debug)
	private final Map<Integer,Double> mapRetrievalProb;  // (debug)
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public Outcome(Model model) {
		this.model = model;
		
		if (model.isDebug()) {
			mapActivation    = new HashMap<Integer,Double>();
			mapRetrievalProb = new HashMap<Integer,Double>();
		}
		else {
			mapActivation    = null;
			mapRetrievalProb = null;
		}
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Calculates activation (A; Anderson & Lebiere, 1998).
	 */
	public double getActivation(int trial) {
		//System.out.println("        Activation");
		
		// (1) Gamma:
		double gamma;
		if (!mapGamma.containsKey(trial)) {  // generate a new gamma
			gamma = Math.random();
			mapGamma.put(trial, gamma);
		}
		else {  // use previously generated gamma
			gamma = mapGamma.get(trial);
		}
		
		// (2) Equation component A:
		double a = model.getNoise() * Math.log((1 - gamma) / gamma);
		
		// (3) Equation component B:
		double b = 0;
		for (int i = 0; i < lstTrial.size(); i++) {
			if (lstTrial.get(i) >= trial) break;  // no more trials from before 'trial'
			b += Math.pow(trial - lstTrial.get(i), -model.getDecay());
		}
		b = Math.log(b);
		
		//System.out.println("          gamma: " + gamma);
		//System.out.println("          a: " + a);
		//System.out.println("          b: " + b);
		//System.out.println("        A: " + (a + b));
		
		double A = a + b;
		if (model.isDebug()) mapActivation.put(trial, A);
		
		return A;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public Map<Integer,Double> getActivations()    { return mapActivation;    }
	public Map<Integer,Double> getGammas()         { return mapGamma;         }
	public Map<Integer,Double> getRetrievalProbs() { return mapRetrievalProb; }
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Should be called whenever an outcome is observed.
	 */
	public void remember(int trial) {
		lstTrial.add(trial);
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public void setRetrievalProb(int trial, double p) { mapRetrievalProb.put(trial, p); }
}


/*
% globalPos - current simulation step
% AnInstance  - the instance for which the activation is calculated
function Activation = CalculateBll(globalPos, AnInstance, d, s) 
activationSum = 0.0;

if not(isempty( AnInstance.USEINDEX))
    for i = 1:length(AnInstance.USEINDEX)
            activationSum = activationSum + ((globalPos - AnInstance.USEINDEX(i)) ^ (-d));
    end;
     p = 0.0001 + (0.9999-0.0001)*rand;
     epsilon = s * log((1 - p) / p);
    Activation = log(activationSum) + epsilon;
else
    Activation = -100;
end;
*/
