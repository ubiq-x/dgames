package main;


/**
 * @author Tomek D. Loboda
 */
public class Result {
	private final boolean ok;
	private final String msg;
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public Result(boolean ok, StringBuffer msg) {
		this.ok = ok;
		this.msg = msg.toString();
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public Result(boolean ok, StringBuilder msg) {
		this.ok = ok;
		this.msg = msg.toString();
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public Result(boolean ok, String msg) {
		this.ok = ok;
		this.msg = msg;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public StringBuffer getMsg(boolean js) {
		return 
			(js
				? new StringBuffer("{outcome:" + ok + (msg.length() > 0 ? "," + msg : "") + "}")
				: new StringBuffer(msg)
			);
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public boolean isOk() { return ok; }
}
