package main;

import inter.DBI;
import inter.JSI;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import util.$;


/**
 * A superclass.
 * 
 * @author Tomek D. Loboda
 */
public abstract class BlockModel {
	protected final long modelId;
	protected final int trialCnt;
	
	protected final String txtTop;
	protected final String txtPayoffPre;
	protected final String txtPayoffPost;
	protected final String lblBtnPrev;
	protected final String lblBtnNext;
	
	protected final List<Decision> lstDecisions = new ArrayList<Decision>();
		// This list stores decision so that they can be easily accessed and manipulate. One such manipulation is order randomization. 
		// The client should submit the index of the decision that is the index in this list and retrieves the actual decision.
	
	protected int    lastDecisionIdx = -1;
	protected double lastOutcome     = 0d;
	
	protected String decisionOrd = null;  // e.g., "0,1" or "3,0,2,1"
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * If 'decisionOrd' is null the method will randomize that order.
	 */
	public BlockModel(long modelId, int trialCnt, String decisionOrd)  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		DBI dbi = null;
		try {
			dbi = new DBI();
			
			this.modelId  = modelId;
			this.trialCnt = trialCnt;
			
			ResultSet rs = dbi.execQry(
				"SELECT " +
					"name, " +
					"p_a_0, " +
					"p_b_0, " +
					"payoff_a_0, " +
					"payoff_a_1, " +
					"payoff_b_0, " +
					"payoff_b_1, " +
					"COALESCE(txt_top, '') AS txt_top, " +
					"COALESCE(txt_btn_a, '') AS txt_btn_a, " +
					"COALESCE(txt_btn_b, '') AS txt_btn_b, " +
					"COALESCE(txt_pre, '') AS txt_pre, " +
					"COALESCE(txt_post, '') AS txt_post, " +
					"COALESCE(txt_payoff_pre, '') AS txt_payoff_pre, " +
					"COALESCE(txt_payoff_post, '') AS txt_payoff_post, " +
					"COALESCE(lbl_btn_prev, '') AS lbl_btn_prev, " +
					"COALESCE(lbl_btn_next, '') AS lbl_btn_next " +
				"FROM model_prob WHERE id = " + modelId
			);
			rs.next();
			
			String txtBtnA, txtBtnB;
			
			String tmp = null;
			tmp = rs.getString("txt_top");         txtTop        = (rs.wasNull() ? null : tmp);
			tmp = rs.getString("txt_btn_a");       txtBtnA       = (rs.wasNull() ? null : tmp);
			tmp = rs.getString("txt_btn_b");       txtBtnB       = (rs.wasNull() ? null : tmp);
			tmp = rs.getString("txt_payoff_pre");  txtPayoffPre  = (rs.wasNull() ? null : tmp);
			tmp = rs.getString("txt_payoff_post"); txtPayoffPost = (rs.wasNull() ? null : tmp);
			tmp = rs.getString("lbl_btn_prev");    lblBtnPrev    = (rs.wasNull() ? null : tmp);
			tmp = rs.getString("lbl_btn_next");    lblBtnNext    = (rs.wasNull() ? null : tmp);
			
			// Decision order:
			// - a fixed decision order:
			if (decisionOrd != null) {
				this.decisionOrd = decisionOrd;
				
				if (decisionOrd.equals("0,1")) {
					lstDecisions.add(new Decision(0, txtBtnA, " "));
					lstDecisions.add(new Decision(1, txtBtnB, " "));
				}
				else {
					lstDecisions.add(new Decision(1, txtBtnB, " "));
					lstDecisions.add(new Decision(0, txtBtnA, " "));
				}
			}
			// - randomize the decision order:
			else {
				lstDecisions.add(new Decision(0, txtBtnA, " "));
				lstDecisions.add(new Decision(1, txtBtnB, " "));
				
				Collections.shuffle(lstDecisions);
				
				String decOrdTmp = "";
				Iterator<Decision> it = lstDecisions.iterator();
				while (it.hasNext()) {
					decOrdTmp += it.next().getName() + (it.hasNext() ? "," : "");
				}
				this.decisionOrd = decOrdTmp;
			}
			
			rs.close();
		}
		finally {
			if (dbi != null) dbi.close();
		}
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public boolean doesStopAtTheEnd() {
		return (trialCnt > 0 && lblBtnNext != null && lblBtnNext.length() > 0);
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public String getDecisionOrd() { return decisionOrd; }
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public String getTxtPayoffPre()  { return txtPayoffPre;  }
	public String getTxtPayoffPost() { return txtPayoffPost; }
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public String getFormattedOutcome(double outcome) {
		return (outcome > 0 ? "+" : "") + (outcome < 0 ? "-" : "") + (txtPayoffPre == null ? "" : txtPayoffPre) + $.dbl2currency(Math.abs(outcome)) + (txtPayoffPost == null ? "" : txtPayoffPost);
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public String getUIActions(int trialIdx, int trialCnt) {
		List<String> res = new ArrayList<String>();
		
		res.add(Sess.CLIENT_ACT_BLOCK_SET_MODEL);
		res.add(Sess.CLIENT_ACT_MODEL_BTN_ENABLED_SET + "(0,true)");
		res.add(Sess.CLIENT_ACT_MODEL_BTN_ENABLED_SET + "(1,true)");
		
		res.add(Sess.CLIENT_ACT_APP_BTN_PREV_LBL_SET + "(null)");
		res.add(Sess.CLIENT_ACT_APP_BTN_NEXT_LBL_SET + "(" + (trialCnt > 0 ? "null" : JSI.str2js(lblBtnNext)) + ")" );
		
		res.add(Sess.CLIENT_ACT_MODEL_TOP_TXT_SET     + "("   + (txtTop                          == null ? "null" : JSI.str2js(txtTop))                          + ")");
		res.add(Sess.CLIENT_ACT_MODEL_BTN_OPT_TXT_SET + "(0," + (lstDecisions.get(0).getTxtBtn() == null ? "null" : JSI.str2js(lstDecisions.get(0).getTxtBtn())) + ")");
		res.add(Sess.CLIENT_ACT_MODEL_BTN_OPT_TXT_SET + "(1," + (lstDecisions.get(1).getTxtBtn() == null ? "null" : JSI.str2js(lstDecisions.get(1).getTxtBtn())) + ")");
		
		res.add(Sess.CLIENT_ACT_MODEL_BTN_OPT_LBL_SET + "(" + lastDecisionIdx + "," + (trialCnt == 0 ? JSI.str2js(getFormattedOutcome(lastOutcome)) : "' '") + ")");
		
		if (doesStopAtTheEnd() && trialIdx == (trialCnt - 1)) {
			res.add(Sess.CLIENT_ACT_MODEL_BTN_ENABLED_SET + "(0,false)");
			res.add(Sess.CLIENT_ACT_MODEL_BTN_ENABLED_SET + "(1,false)");
			
			res.add(Sess.CLIENT_ACT_APP_BTN_NEXT_LBL_SET + "(" + JSI.str2js(lblBtnNext) + ")");
		}
		
		return $.join(res, "", "", ",", false, true).toString();
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	abstract public double getOutcome(int decisionIdx, long decisionT, long sessId, long blockId, int trialIdxSess, int trialIdxBlock, double outcomeTotal, boolean doUpdOut)  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException;
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public void setBtnOptLbl(int decisionIdx, String lbl) {
		switch (decisionIdx) {
			case 0:
				lstDecisions.get(0).setLblBtn(lbl);
				lstDecisions.get(1).setLblBtn(null);
				break;
				
			case 1:
				lstDecisions.get(0).setLblBtn(null);
				lstDecisions.get(1).setLblBtn(lbl);
				break;
		}
	}
}
