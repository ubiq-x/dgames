package main;

import inter.DBI;
import inter.JSI;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import util.$;


/**
 * @author Tomek D. Loboda
 */
public class BlockHTML {
	private final String html;
	private final String url;
	private final String jsFnPrev;
	private final String jsFnNext;
	private final String lblBtnPrev;
	private final String lblBtnNext;
	
	//private final int minReadTime;  // [ms]
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public BlockHTML(long htmlId)  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		DBI dbi = null;
		try {
			dbi = new DBI();
			
			ResultSet rs = dbi.execQry("SELECT html, url, js_fn_prev, js_fn_next, lbl_btn_prev, lbl_btn_next, min_read_time FROM html WHERE id = " + htmlId);
			rs.next();
			
			String tmp = null;
			tmp = rs.getString("html");          html       = (rs.wasNull() ? null : tmp);
			tmp = rs.getString("url");           url        = (rs.wasNull() ? null : tmp);
			tmp = rs.getString("js_fn_prev");    jsFnPrev   = (rs.wasNull() ? null : tmp);
			tmp = rs.getString("js_fn_next");    jsFnNext   = (rs.wasNull() ? null : tmp);
			tmp = rs.getString("lbl_btn_prev");  lblBtnPrev = (rs.wasNull() ? null : tmp);
			tmp = rs.getString("lbl_btn_next");  lblBtnNext = (rs.wasNull() ? null : tmp);
			
			//minReadTime = rs.getInt("min_read_time");
			
			rs.close();
		}
		finally {
			if (dbi != null) dbi.close();
		}
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public String getUIActions() {
		List<String> res = new ArrayList<String>();
		
		res.add(Sess.CLIENT_ACT_BLOCK_SET_HTML);
		
		if (html != null) res.add(Sess.CLIENT_ACT_HTML_HTML_SET + "(" + JSI.str2js(html) + "," + JSI.str2js(jsFnPrev) + "," + JSI.str2js(jsFnNext) + ")");
		if (url  != null) res.add(Sess.CLIENT_ACT_HTML_URL_SET  + "(" + JSI.str2js(url)  + "," + JSI.str2js(jsFnPrev) + "," + JSI.str2js(jsFnNext) + ")");
		
		res.add(Sess.CLIENT_ACT_APP_BTN_PREV_LBL_SET + "(" + (lblBtnPrev == null ? "null" : JSI.str2js(lblBtnPrev)) + ")");
		res.add(Sess.CLIENT_ACT_APP_BTN_NEXT_LBL_SET + "(" + (lblBtnNext == null ? "null" : JSI.str2js(lblBtnNext)) + ")");
		
		return $.join(res, "", "", ",", false, true).toString();
	}
}
