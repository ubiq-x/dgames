package main;

import inter.DBI;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.apache.http.ParseException;
import org.xml.sax.SAXException;

import exception.LogicException;
import exception.ParamException;


/**
 * 
 * @author Tomek D. Loboda
 */
public class Sess {
	public static final String CLIENT_ACT_APP_BTN_PREV_LBL_SET = "doAppBtnPrevLblSet";
	public static final String CLIENT_ACT_APP_BTN_NEXT_LBL_SET = "doAppBtnNextLblSet";
	public static final String CLIENT_ACT_APP_VAR_SET          = "doAppVarSet";
	public static final String CLIENT_ACT_APP_AMT_SHOW_BTN     = "doAppAmtShowBtn()";
	
	public static final String CLIENT_ACT_BLOCK_SET_HTML  = "doBlockSetHtml()";
	public static final String CLIENT_ACT_BLOCK_SET_VAR   = "doBlockSetVar()";
	public static final String CLIENT_ACT_BLOCK_SET_MODEL = "doBlockSetModel()";
	
	public static final String CLIENT_ACT_HTML_HTML_SET = "doHtmlHtmlSet";
	public static final String CLIENT_ACT_HTML_URL_SET  = "doHtmlUrlSet";
	
	public static final String CLIENT_ACT_MODEL_TOP_TXT_SET     = "doModelTopTxtSet";
	public static final String CLIENT_ACT_MODEL_BTN_OPT_TXT_SET = "doModelBtnOptTxtSet";
	public static final String CLIENT_ACT_MODEL_BTN_OPT_LBL_SET = "doModelBtnOptLblSet";
	public static final String CLIENT_ACT_MODEL_BTN_ENABLED_SET = "doModelBtnOptEnabledSet";
	
	public static final String CLIENT_ACT_VAR_SET = "doVarSet";
	
	public static final String MSG_ERR_SESS_ENDED = "Session has ended.";
	public static final String MSG_ERR_EXP_FULL   = "We are sorry, but this experiment is full at this point.";
	public static final String MSG_ERR_SUB_EXISTS = "Our records indicate that you have already completed this experiment. We are sorry, but you can participate in it more than once.";
	
	private final boolean isSim;  // flag: is a simulated subject session?
	private       boolean isEnded = false;
	private       boolean isValid = false;
	
	private final long expId;
	private final long subId;
	private final long condId;
	private final long sessId;
	
	private final int  blockCnt;      // number of blocks in the assigned condition
	private       int  blockIdx = 0;  // once the block has been completed, a subsequent block (if it exists) will be presented to the subject
	
	private int trialIdxSess  =  0;  // index of the trial within the session (ever-increasing) 
	
	private long tLastAccess = 0;  // time of last access (used only to keep track of life span of simulated subject sessions; real subject sessions life span is controlled by the HTTP session)
	private long timeout     = 0;  // session timeout (ditto) [ms]
	
	private Double outcomeLast         = null;  // outcome from the most recent trial
	private double outcomeInit         = 0;     // initial outcome (in-game currency)
	private double outcomeTotal        = 0;     // total outcome of the session (in-game currency)
	private int    outcomeAmtBase      = 0;     // Amazon Mechanical Turk base compensation [US cents]
	private int    outcomeAmtBonusMax  = 0;     // Amazon Mechanical Turk max. compensation (a security valve: automatic payment will not be issued if the final compensation calculated at the end of the experiment is higher than this number) [US cents]
	private int    outcomeAmtBonus     = 0;     // Amazon Mechanical Turk bonus compensation [US cents]
	private int    outcomeAmtTotal     = 0;     // Amazon Mechanical Turk total compensation (outcomeAMTBase + outcomeAMTBonus) [US cents]
	private double outcomeAmtBonusMult = 0;     // Amazon Mechanical Turk bonus multiplier (outcomeTotal * outcomeAMTBonusMult = bonus compensation)
	
	private Block block = null;  // the current block (a subject goes through a series of blocks associated with the condition they get assigned to)
	
	private String decisionOrd = null;  // the order of decisions presented to the subject (I keep it as a session variable to ensure that once the order has been established it remains unchanged until order randomization has been requested, in which case this variable is also updated)
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public Sess(long expId, String amtWorkerId, String amtAssignId, boolean isSim)  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException, LogicException {
		Exp.checkIdValid(expId);
		
		this.isSim = isSim;
		this.expId = expId;
		
		// Assign to a condition, create a new subject, and create a new session:
		DBI dbiX = null;
		try {
			dbiX = new DBI();
			
			if (amtWorkerId != null && DBI.getLong(dbiX, "SELECT COUNT(*) FROM sub WHERE exp_id = " + expId + " AND amt_worker_id = " + DBI.esc(amtWorkerId)) > 0) throw new LogicException(MSG_ERR_SUB_EXISTS);
			
			Statement stX = dbiX.transBegin();
			
			try {
				condId = assignToCond(stX);
				if (condId == DBI.ID_NONE) {
					dbiX.transRollback();
					throw new LogicException(MSG_ERR_EXP_FULL);
				}
				
				subId  = DBI.insertGetId(stX, null, "INSERT INTO sub (exp_id, is_sim, amt_worker_id) VALUES (" + expId + "," + isSim + "," + (amtWorkerId == null ? "null" : DBI.esc(amtWorkerId)) + ")");
				sessId = DBI.insertGetId(stX, null, "INSERT INTO sess (sub_id, cond_id, amt_assign_id) VALUES (" + subId + "," + condId + "," + (amtAssignId == null ? "null" : DBI.esc(amtAssignId)) + ")");
				
				ResultSet rsCond = dbiX.execQry("SELECT out_init, amt_pay_bonus_max, amt_pay_bonus_mult, timeout FROM cond WHERE id = " + condId);
				rsCond.next();
				outcomeInit         = rsCond.getDouble("out_init");
				outcomeTotal        = outcomeInit;
				outcomeAmtBonusMax  = rsCond.getInt("amt_pay_bonus_max");
				outcomeAmtBonusMult = rsCond.getDouble("amt_pay_bonus_mult");
				timeout             = rsCond.getInt("timeout") * 1000;
				rsCond.close();
				
				outcomeAmtBase = DBI.getLong(dbiX, "SELECT amt_pay_base FROM exp WHERE id = " + expId).intValue();
				
				blockCnt = (int) DBI.getLong(stX, "SELECT COUNT(*) FROM block WHERE cond_id = " + condId);
				block = new Block(condId, sessId, 0, decisionOrd);
				decisionOrd = block.getDecisionOrd();
				
				dbiX.transCommit();
			}
			catch (SQLException e) {
				dbiX.transRollback();
				throw e;
			}
		}
		finally {
			if (dbiX != null) dbiX.close();
		}
		
		tLastAccess = System.currentTimeMillis();
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Assigns the subject to a condition. Returns the condition ID.
	 */
	private long assignToCond(Statement stX)  throws SQLException {
		String qryCondLst = Cond.getQry_lst(expId, isSim);
		
		int subCntMin = (int) DBI.getLong(stX, "SELECT MIN(sub_cnt_curr) FROM (" + qryCondLst + ") a");
		long condId = DBI.getLong(stX, "SELECT id FROM (" + qryCondLst + ") a WHERE sub_cnt_curr < sub_cnt_max AND sub_cnt_curr = " + subCntMin + " ORDER BY RANDOM() LIMIT 1");
		
		return condId;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public void end()  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException, KeyManagementException, UnrecoverableKeyException, KeyStoreException, NoSuchAlgorithmException, CertificateException, SignatureException, UnsupportedEncodingException, ParseException, XPathExpressionException, IOException, ParserConfigurationException, SAXException {
		DBI dbi = null;
		try {
			dbi = new DBI();
			dbi.execUpd("UPDATE sess SET is_ended = true, is_valid = " + isValid + " WHERE id = " + sessId);
		}
		finally {
			if (dbi != null) dbi.close();
		}
		
		isEnded = true;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Executes subject's action.
	 * 
	 * @param action
	 *   - make-dec  : make decision
	 *   - end-block : advance to the next block
	 * 
	 * Not all of the above actions are allowed at any given stage of the session so state check and conditional 
	 * execution is necessary.
	 */
	public Result execAction(ReqCtx rc)  throws SQLException, ParamException, InstantiationException, IllegalAccessException, ClassNotFoundException, KeyManagementException, UnrecoverableKeyException, KeyStoreException, NoSuchAlgorithmException, CertificateException, SignatureException, ParseException, XPathExpressionException, IOException, ParserConfigurationException, SAXException {
		if (hasEnded()) return new Result(false, "msg:\"" + MSG_ERR_SESS_ENDED + "\"");
		
		tLastAccess = System.currentTimeMillis();
		
		String action = rc.getStr("action", true);
		if (action.equals("model-make-decision")) {
			trialIdxSess++;
			
			int decisionIdx = rc.getInt("dec-idx", true, 0, 1);  // TODO: change 'max' for more decisions
			long decisionT  = rc.getLong("dec-t", true, 0l, null);
			
			outcomeLast = block.modelMakeDecision(decisionIdx, decisionT, trialIdxSess, outcomeTotal);
			if (outcomeLast != null) {
				if (block.doUpdOut()) {
					outcomeTotal += outcomeLast;
					outcomeAmtBonus = (int) Math.round(outcomeTotal * outcomeAmtBonusMult);
					outcomeAmtTotal = outcomeAmtBase + outcomeAmtBonus;
				}
				block.setBtnOptLbl(decisionIdx, block.getFormattedOutcome(outcomeLast));
			}
			
			if (block.hasEnded()) {
				block.end(rc);
				loadBlock(++blockIdx);
			}
		}
		else if (action.equals("block-next")) {
			block.end(rc);
			loadBlock(++blockIdx);
		}
		else if (action.equals("block-prev")) {
			// ...
		}
		else return new Result(false, "msg:\"Unknown action specified.\"");
		
		hasEnded();
		
		return getState();
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public long getExpId()  { return expId;  }
	public long getSubId()  { return subId;  }
	public long getCondId() { return condId; }
	public long getSessId() { return sessId; }
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Returns the current state of the session. This information is used to both sync the session state on the client 
	 * side and to initialize the client session properly in case the client has refreshed the page.
	 */
	public Result getState()  throws SQLException {
		String blockUIActions = block.getUIActions();
		String res =
			"sessId:" + sessId + "," +
			"exec:[" +
			CLIENT_ACT_APP_VAR_SET + "('sessId',"             + sessId          + ")," +
			CLIENT_ACT_APP_VAR_SET + "('outcomeInit',"        + outcomeInit     + ")," +
			CLIENT_ACT_APP_VAR_SET + "('outcomeLast',"        + outcomeLast     + ")," +
			CLIENT_ACT_APP_VAR_SET + "('outcomeTotal',"       + outcomeTotal    + ")," +
			CLIENT_ACT_APP_VAR_SET + "('outcomeAmtBase',"     + outcomeAmtBase  + ")," +
			CLIENT_ACT_APP_VAR_SET + "('outcomeAmtBonus',"    + outcomeAmtBonus + ")," +
			CLIENT_ACT_APP_VAR_SET + "('outcomeAmtBonusMax'," + outcomeAmtBonusMax   + ")," +
			CLIENT_ACT_APP_VAR_SET + "('outcomeAmtTotal',"    + outcomeAmtTotal + ")"  +
			//(blockIdx == (blockCnt - 1) ? "," + CLIENT_ACT_APP_AMT_SHOW_BTN : "") + 
			(blockUIActions != null && blockUIActions.length() > 0 ? "," : "") + blockUIActions +
			"]" +
			(isSim ? ",subId:" + subId + ",condId:" + condId + ",timeout:" + (timeout/1000) + ",tIdle:" + Math.round((System.currentTimeMillis() - tLastAccess) / 1000) : "");
		
		return new Result(true, res);
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Returns the status of the session which is used mostly for debugging and experiment simulation purposes.
	 */
	public Result getStatus()  throws SQLException  {
		if (isEnded) return new Result(false, "msg:\"" + MSG_ERR_SESS_ENDED + "\"");
		
		if (condId == DBI.ID_NONE) return new Result(false, "msg:\"The experiment is full.\"");
		
		long tIdle = System.currentTimeMillis() - tLastAccess;
		return new Result(true, "sessId:" + sessId + ",subId:" + subId + ",condId:" + condId + ",timeout:" + (timeout/1000) + ",tIdle:" + Math.round(tIdle/1000));
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Returns the status of the session which is used mostly for debugging and experiment simulation purposes.
	 */
	public String getStatusStr()  throws SQLException {
		if (isEnded) return MSG_ERR_SESS_ENDED;
		
		long tIdle = System.currentTimeMillis() - tLastAccess;
		return (condId == DBI.ID_NONE
			? "msg:\"The experiment is full.\""
			: "sessId:" + sessId + ",subId:" + subId + ",condId:" + condId + ",timeout:" + (timeout/1000) + ",tIdle:" + Math.round(tIdle/1000)
		);
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Ensures that the database is in a logically consistent state by appropriately marking the associated session as 
	 * valid or invalid upon the destruction of this session object.
	 */
	protected void finalize()  throws Throwable {
		super.finalize();
		end();
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Checks if the session has ended and if necessary propagates appropriate information to the database.
	 */
	public boolean hasEnded()  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException, KeyManagementException, UnrecoverableKeyException, KeyStoreException, NoSuchAlgorithmException, CertificateException, SignatureException, UnsupportedEncodingException, ParseException, XPathExpressionException, IOException, ParserConfigurationException, SAXException {
		if (isEnded) return true;
		
		long tIdle = System.currentTimeMillis() - tLastAccess;
		if (tIdle > timeout) {
			end();
			return true;
		}
		
		if (blockIdx == (blockCnt - 1)) {
			isValid = true;
			end();
			return true;
		}
		
		return false;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Loads the block with the specified order (if exists) and all of the associated model information. If that block 
	 * does not exist, that means the session has been successfully ended. In such event, this information is 
	 * propagated to the database.
	 */
	private void loadBlock(int blockIdx)  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException, KeyManagementException, UnrecoverableKeyException, KeyStoreException, NoSuchAlgorithmException, CertificateException, SignatureException, UnsupportedEncodingException, ParseException, XPathExpressionException, IOException, ParserConfigurationException, SAXException {
		this.blockIdx = blockIdx;
		if (blockIdx < blockCnt) {
			block = new Block(condId, sessId, blockIdx, decisionOrd);
			decisionOrd = block.getDecisionOrd();
		}
		else hasEnded();
	}
}
