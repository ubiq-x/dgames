package main;

public class Decision {
	private final int name;
	private final String txtBtn;
	private       String lblBtn;
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public Decision(int name, String txtBtn, String lblBtn) {
		this.name   = name;
		this.txtBtn = txtBtn;
		this.lblBtn = lblBtn;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public int    getName()   { return name;    }
	public String getTxtBtn() { return txtBtn; }
	public String getLblBtn() { return lblBtn; }
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public void setLblBtn(String lblBtn) { this.lblBtn = lblBtn; }
}
