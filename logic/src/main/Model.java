/**
 * Instance Based Learning (IBL) model.
 *
 * References (for this and all dependent classes):
 *   Anderson, J. R., & Lebiere, C. (1998). The atomic components of thought. Mahwah, NJ: Erlbaum.
 *   Gonzalez, C., Lerch, F. J., & Lebiere, C. (2003). Instance-based learning in real-time dynamic decision making. Cognitive Science, 27, 591-635.
 *   Lebiere, C. (1999). Blending. In Proceedings of the Sixth ACT-R Workshop, Fairfax, VA.
 *   Lejarraga, T., Dutt, B, & Gonzalez, C. (2010). Instance-based Learning: A General Model of Repeated Binary Choice. Journal of Behavioral Decision Making, J. Behav. Dec. Making.
 */

package main;

import inter.FSI;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import util.$;
import util.Stats;


/**
 * The Instance Based Learning (IBL; Gonzalez, Lerch, & Lebiere, 2003) model.
 *
 * @author Tomek D. Loboda
 */
public class Model {
	final private int decisionCnt;

	final private double noise;
	final private double decay;

	final private List<Instance> lstInstance = new ArrayList<Instance>();
	final private List<IBLDecision> lstDecision = new ArrayList<IBLDecision>();
		// Stores all the decision options (e.g., there will be two options for the Prisoner's Dilemma game). These
		// options should be integer-indexed and zero-based. This indexing convention can be easily changed.

	final private boolean isDebug;


	// -----------------------------------------------------------------------------------------------------------------
	public Model(int decisionCnt, double noise, double decay, boolean isDebug) {
		this.noise = noise;
		this.decay = decay;

		this.decisionCnt = decisionCnt;
		this.isDebug = isDebug;

		eraseMemory();

		//System.out.println("Model");
		//System.out.println("  noise: " + noise);
		//System.out.println("  decay: " + decay);
		//System.out.println("  decisionCnt: " + decisionCnt);
	}


	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Makes the model forget all its past experiences (i.e., instances it has seen).
	 */
	public void eraseMemory() {
		lstInstance.clear();
		lstDecision.clear();

		for (int i = 0; i < decisionCnt; i++) {
			IBLDecision d = new IBLDecision(this);
			lstDecision.add(d);

			d.remember(30, 0);  // pre-populated outcome
		}
	}


	// -----------------------------------------------------------------------------------------------------------------
	double getDecay() { return decay; }
	double getNoise() { return noise; }


	// -----------------------------------------------------------------------------------------------------------------
	List<IBLDecision> getDecisions() { return lstDecision; }


	// -----------------------------------------------------------------------------------------------------------------
	public boolean isDebug() { return isDebug; }


	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Makes the model make a decision. That decision is based on the entirety of its experience (i.e., the vector of
	 * instances it has seen) and its parameters.
	 */
	public int makeDecision(int time) {
		//System.out.println("Making decision (time: " + time + ")");
		double bvMax = Double.MIN_VALUE;  // maximum blended value

		int decision = 0;
		for (int i = 0; i < decisionCnt; i++) {
			//System.out.println("  Decision: " + i);
			double bv = lstDecision.get(i).getBlendedValue(time);
			if (bv > bvMax) {
				bvMax = bv;
				decision = i;
			}
			//System.out.println("  V: " + bv);
		}
		//System.out.println("done");

		return decision;
	}


	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Makes the model remember the specified decision (i.e., store it in the form of an instance).
	 */
	public void rememberInstance(int time, int decision, float outcome) {
		lstInstance.add(new Instance(time, decision, outcome));
		lstDecision.get(decision).remember(outcome, time);
	}


	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Plays a set of 1x2 games (i.e., the model plays alone a game with two options and four possible outcomes). Games
	 * should appear in the game specification file one per line and the line format is as follows:
	 *
	 *     <game-id>,<outcome00>,<p00>,<outcome01>,<outcome10>,<p10>,<outcome11>
	 *
	 * A sample line from the 'estimationset.csv' file:
	 *
	 *     1,-0.3,0.96,-2.1,-0.3,1,0
	 *
	 * Results are tracked on the following levels (the 'resXX' variable):
	 *
	 *     1. Model and setup information (res01)
	 *     2. Game-set-wide statistics (res05)
	 *     3. Game-wide statistics (res04)
	 *     4. Decisions and outcomes (res03)
	 *     5. Internal model computations, e.g., activation values (res02)
	 */
	public String playSet1x2(String filename, String delim, int trialMax)  throws IOException {
		StringBuffer res01 = new StringBuffer("1. Model and setup information\n\n");
		StringBuffer res02 = new StringBuffer("2. Game-set-wide statistics\n\n");
		StringBuffer res03 = new StringBuffer("3. Game-wide statistics\n\ngame-id\tdec-n-0\tdec-n-1\tdec-p-0\tdec-p-1\n");
		StringBuffer res04 = new StringBuffer("4. Decisions and outcomes\n\nstart\tgame-id\ttrial\tdec\tout\n");
		StringBuffer res05 = new StringBuffer("5. Internal model computations\n\nstart\tgame-id\ttrial\tdec\tout\tV0\tV1\tp00\tp01\tp02\tp10\tp11\tp12\tA00\tA01\tA02\tA10\tA11\tA12\n");

		res01.append(
			"Decision count: " + decisionCnt + "\n" +
			"Noise (sigma): " + noise  + "\n" +
			"Decay (d): " + decay + "\n" +
			"\n" +
			"Trial max: " + trialMax + "\n"
		);

		Stats sDecCntGameSet0 = new Stats();
		Stats sDecCntGameSet1 = new Stats();

		List<String> L = FSI.file2lst(filename);  // lines

		for (int i = 0; i < L.size(); i++) {  // for each game
			String l = L.get(i);
			if (l.trim().length() == 0) continue;

			int decCntGame0 = 0;
			int decCntGame1 = 0;

			// (1) Decode the line:
			String[] A = l.split(delim);
			if (A.length != 7) return "Line " + i + " has too few arguments";

			String gameId = A[0];
			float gameOutcome00 = $.str2float(A[1].trim(), null, null);
			float gameOutcome01 = $.str2float(A[3].trim(), null, null);
			float gameOutcome10 = $.str2float(A[4].trim(), null, null);
			float gameOutcome11 = $.str2float(A[6].trim(), null, null);

			float gameP00 = $.str2float(A[2].trim(), null, null);
			float gameP10 = $.str2float(A[5].trim(), null, null);

			// (2) Play the game:
			eraseMemory();

			for (int t = 1; t <= trialMax; t++) {
				int decision = makeDecision(t);

				double rand = Math.random();
				float outcome = Float.MIN_VALUE;
				switch (decision) {
					case 0:
						outcome = (rand <= gameP00 ? gameOutcome00 : gameOutcome01);
						decCntGame0++;
						break;
					case 1:
						outcome = (rand <= gameP10 ? gameOutcome10 : gameOutcome11);
						decCntGame1++;
						break;
				}

				rememberInstance(t, decision, outcome);
			}

			sDecCntGameSet0.addValue((double) decCntGame0 / trialMax);
			sDecCntGameSet1.addValue((double) decCntGame1 / trialMax);

			// (3) Populate results:
			// (3.1) Internal model computations:
			Map<Float,Outcome> outcomes0 = lstDecision.get(0).getOutcomes();
			Map<Float,Outcome> outcomes1 = lstDecision.get(1).getOutcomes();

			res05.append("*\tOutcomes 0: (");
			for (int j = 0; j < outcomes0.size(); j++) {
				res05.append(j + ":" + outcomes0.keySet().toArray()[j] + (j < outcomes0.size() - 1 ? ", " : ""));
			}
			res05.append(")  Outcomes 1: (");
			for (int j = 0; j < outcomes1.size(); j++) {
				res05.append(j + ":" + outcomes1.keySet().toArray()[j] + (j < outcomes1.size() - 1 ? ", " : ""));
			}
			res05.append(")\n");

			for (Instance instance: lstInstance) {
				int trial = instance.getTrial();

				Double V0=null, V1=null, p00=null, p01=null, p02=null, p10=null, p11=null, p12=null, A00=null, A01=null, A02=null, A10=null, A11=null, A12=null;

				V0 = $.trunc(lstDecision.get(0).getBlendedValues().get(trial), 2);
				V1 = $.trunc(lstDecision.get(1).getBlendedValues().get(trial), 2);

				/*
				System.out.print(gameId + "," + trial + "," + outcomes0.size() + "-" + outcomes1.size() + ",");
				System.out.print(outcomes0.get(outcomes0.keySet().toArray()[0]) + "-");
				System.out.print(outcomes0.get(outcomes0.keySet().toArray()[1]) + "-");
				System.out.print(outcomes0.get(outcomes0.keySet().toArray()[2]) + ",");
				System.out.print(outcomes1.get(outcomes1.keySet().toArray()[0]) + "-");
				System.out.print(outcomes1.get(outcomes1.keySet().toArray()[1]) + "-");
				System.out.print(outcomes1.get(outcomes1.keySet().toArray()[2]) + ",");
				System.out.println();
				*/

				p00 = (outcomes0.size() > 0 ? outcomes0.get(outcomes0.keySet().toArray()[0]).getRetrievalProbs().get(trial) : null);
				p01 = (outcomes0.size() > 1 ? outcomes0.get(outcomes0.keySet().toArray()[1]).getRetrievalProbs().get(trial) : null);
				p02 = (outcomes0.size() > 2 ? outcomes0.get(outcomes0.keySet().toArray()[2]).getRetrievalProbs().get(trial) : null);

				p10 = (outcomes1.size() > 0 ? outcomes1.get(outcomes1.keySet().toArray()[0]).getRetrievalProbs().get(trial) : null);
				p11 = (outcomes1.size() > 1 ? outcomes1.get(outcomes1.keySet().toArray()[1]).getRetrievalProbs().get(trial) : null);
				p12 = (outcomes1.size() > 2 ? outcomes1.get(outcomes1.keySet().toArray()[2]).getRetrievalProbs().get(trial) : null);

				A00 = (outcomes0.size() > 0 ? outcomes0.get(outcomes0.keySet().toArray()[0]).getActivations().get(trial) : null);
				A01 = (outcomes0.size() > 1 ? outcomes0.get(outcomes0.keySet().toArray()[1]).getActivations().get(trial) : null);
				A02 = (outcomes0.size() > 2 ? outcomes0.get(outcomes0.keySet().toArray()[2]).getActivations().get(trial) : null);

				A10 = (outcomes1.size() > 0 ? outcomes1.get(outcomes1.keySet().toArray()[0]).getActivations().get(trial) : null);
				A11 = (outcomes1.size() > 1 ? outcomes1.get(outcomes1.keySet().toArray()[1]).getActivations().get(trial) : null);
				A12 = (outcomes1.size() > 2 ? outcomes1.get(outcomes1.keySet().toArray()[2]).getActivations().get(trial) : null);

				res05.append(
					"\t" +
					gameId + "\t" +
					instance.getTrial() + "\t" +
					instance.getDecision() + "\t" +
					$.trunc(instance.getOutcome(), 3) + "\t" +
					(V0  == null ? "" : (V0  < 0 ? "" : "") + $.trunc(V0, 2)) + "\t" +
					(V1  == null ? "" : (V1  < 0 ? "" : "") + $.trunc(V1, 2)) + "\t" +
					(p00 == null ? "" :                        $.trunc(p00, 2)) + "\t" +
					(p01 == null ? "" :                        $.trunc(p01, 2)) + "\t" +
					(p02 == null ? "" :                        $.trunc(p02, 2)) + "\t" +
					(p10 == null ? "" :                        $.trunc(p10, 2)) + "\t" +
					(p11 == null ? "" :                        $.trunc(p11, 2)) + "\t" +
					(p12 == null ? "" :                        $.trunc(p12, 2)) + "\t" +
					(A00 == null ? "" : (A00 < 0 ? "" : "") + $.trunc(A00, 2)) + "\t" +
					(A01 == null ? "" : (A01 < 0 ? "" : "") + $.trunc(A01, 2)) + "\t" +
					(A02 == null ? "" : (A02 < 0 ? "" : "") + $.trunc(A02, 2)) + "\t" +
					(A10 == null ? "" : (A10 < 0 ? "" : "") + $.trunc(A10, 2)) + "\t" +
					(A11 == null ? "" : (A11 < 0 ? "" : "") + $.trunc(A11, 2)) + "\t" +
					(A12 == null ? "" : (A12 < 0 ? "" : "") + $.trunc(A12, 2)) + "\n"
				);
			}

			// (3.2) Decisions and outcomes:
			boolean gameStart = true;
			for (Instance instance: lstInstance) {
				res04.append((gameStart ? "*" : "") + "\t" + gameId + "\t" + instance.getTrial() + "\t" + instance.getDecision() + "\t" + $.trunc(instance.getOutcome(), 2) + "\n");
				gameStart = false;
			}

			// (3.3) Game-wide statistics:
			res03.append(gameId + "\t" + decCntGame0 + "\t" + $.trunc((double) decCntGame0 / trialMax, 2) + "\t" + decCntGame1 + "\t" + $.trunc((double) decCntGame1 / trialMax, 2) + "\n");
		}

		// (4) Game-set-wide statistics:
		res02.append(
			"Decision 0 (proportion): mean=" + sDecCntGameSet0.getMean(2) + ", sd=" + sDecCntGameSet0.getStdev(2) + ", se=" + sDecCntGameSet0.getSterr(2) + ", median=" + sDecCntGameSet0.getMedian(2) + ", min=" + sDecCntGameSet0.getMin(2) + ", max=" + sDecCntGameSet0.getMax(2) + "\n" +
			"Decision 1 (proportion): mean=" + sDecCntGameSet1.getMean(2) + ", sd=" + sDecCntGameSet1.getStdev(2) + ", se=" + sDecCntGameSet0.getSterr(2) + ", median=" + sDecCntGameSet1.getMedian(2) + ", min=" + sDecCntGameSet1.getMin(2) + ", max=" + sDecCntGameSet1.getMax(2) + "\n"
		);

		// (5) Finish:
		res01.append("\n\n");
		res02.append("\n\n");
		res03.append("\n\n");
		res04.append("\n\n");

		return res01.toString() + res02.toString() + res03.toString() + res04.toString() + res05.toString();
	}
}
