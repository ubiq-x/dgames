package main;


/**
 * Stores information about instances observed in a course of a game. Each instance, as assumed by the IBLT, consists 
 * of three components: situation, decision, and outcome. This object is not used in calculations and is used to 
 * store the sequntial information about the coincidence of a decision and an outcome.
 * 
 * @author Tomek D. Loboda
 */
public class Instance {
	// ... (situation)
	final private int    trial;
	final private int    decision;
	final private double outcome;
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public Instance(int trial, int decision, double outcome) {
		this.trial    = trial;
		this.decision = decision;
		this.outcome  = outcome;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public int    getTrial()    { return trial;     }
	public int    getDecision() { return decision; }
	public double getOutcome()  { return outcome;  }
}
