package main;

import inter.DBI;

import java.io.UnsupportedEncodingException;
import java.sql.ResultSet;
import java.sql.SQLException;

import exception.ParamException;


/**
 * @author Tomek D. Loboda
 */
public class Block {
	public static enum BlockType { UNKNOWN, HTML, MODEL_PROB, MODEL_IBL, VAR };
	
	private final BlockType type;
	
	private final long condId;
	private final long blockId;
	private final long sessId;
	
	private int trialCnt;
	private int trialIdx = 0;
	
	private final BlockModel blockModel;
	private final BlockHTML  blockHtml;
	private final BlockVar   blockVar;
	
	private final boolean doUpdOut;
	private final boolean doRandDecOrd;
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public String getDecisionOrd() { return (blockModel == null ? null : blockModel.getDecisionOrd()); }
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Loads the block with the specified order number and returns its ID.
	 */
	public Block(long condId, long sessId, int blockIdx, String decisionOrd)  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		DBI dbi = null;
		try {
			dbi = new DBI();
			
			this.condId = condId;
			this.sessId = sessId;
			
			ResultSet rs = dbi.execQry("SELECT b.id, b.model_prob_id, b.model_ibl_id, b.html_id, b.var_id, b.trial_cnt, b.do_upd_out, b.do_rand_dec_ord FROM block b INNER JOIN cond c ON c.id = b.cond_id WHERE c.id = " + condId + " AND b.idx = " + blockIdx);
			if (rs.next()) {
				blockId      = rs.getLong("id");
				trialCnt     = rs.getInt("trial_cnt");
				doUpdOut     = rs.getBoolean("do_upd_out");
				doRandDecOrd = rs.getBoolean("do_rand_dec_ord");
				
				long htmlId      = rs.getLong("html_id");
				long modelProbId = rs.getLong("model_prob_id");
				long modelIBLId  = rs.getLong("model_ibl_id");
				long varId       = rs.getLong("var_id");
				
				if (htmlId != DBI.ID_NONE) {
					type = BlockType.HTML;
					
					blockModel = null;
					blockHtml  = new BlockHTML(htmlId);
					blockVar   = null;
					
					// ...
				}
				else if (modelProbId != DBI.ID_NONE) {
					type = BlockType.MODEL_PROB;
					
					blockModel = new BlockModelProb(modelProbId, trialCnt, (doRandDecOrd ? null : decisionOrd));
					blockHtml  = null;
					blockVar   = null;
					
					if (trialCnt > 0 && blockModel.doesStopAtTheEnd()) trialCnt++;
						// This increment is to add one extra artificial trial for showing the 'Continue' button to the subject after 
						// they have made a decision in a 1-trial decision block (i.e., in a decision from experience or final 
						// decision situation)
				}
				else if (modelIBLId != DBI.ID_NONE) {
					type = BlockType.MODEL_IBL;
					
					blockModel = null;  // TODO: Instantiate the IBL model
					blockHtml  = null;
					blockVar   = null;
					
					// ...
				}
				else if (varId != DBI.ID_NONE) {
					type = BlockType.VAR;
					
					blockModel = null;
					blockHtml  = null;
					blockVar   = new BlockVar(varId);
					
					// ...
				}
				else {
					type = BlockType.UNKNOWN;
					
					blockModel = null;
					blockHtml  = null;
					blockVar   = null;
				}
			}
			else {
				type         = BlockType.UNKNOWN;
				blockId      = DBI.ID_NONE;
				trialCnt     = 0;
				doUpdOut     = false;
				doRandDecOrd = false;
				blockModel   = null;
				blockHtml    = null;
				blockVar     = null;
			}
			rs.close();
		}
		finally {
			if (dbi != null) dbi.close();
		}
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public boolean canBeEnded() { return trialCnt == 0; }
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public boolean doUpdOut() { return doUpdOut; }
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public void end(ReqCtx rc)  throws SQLException, UnsupportedEncodingException, ParamException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		if (type == BlockType.VAR) {
			String varName = rc.getStr("var-name", false);
			if (blockVar.getName().equals(varName)) blockVar.persist(sessId, rc.getStr("var-val", false));
		}
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public Double modelMakeDecision(int decisionIdx, long decisionT, int trialIdxSess, double outcomeTotal)  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		if (blockModel == null) return null;
		
		trialIdx++;
		return blockModel.getOutcome(decisionIdx, decisionT, sessId, blockId, trialIdxSess, trialIdx, outcomeTotal, doUpdOut);
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public long getCondId()  { return condId;  }
	public long getBlockId() { return blockId; }
	
	public long getTrialCnt() { return trialCnt; }
	public long getTrialIdx() { return trialIdx; }
	
	public BlockModel getModel() { return blockModel; }
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public String getFormattedOutcome(double outcome) {
		return (blockModel == null ? "" : blockModel.getFormattedOutcome(outcome));
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public String getUIActions() {
		switch (type) {
			case HTML       : return blockHtml.getUIActions();
			case MODEL_PROB : return blockModel.getUIActions(trialIdx, trialCnt);
			case MODEL_IBL  : return blockModel.getUIActions(trialIdx, trialCnt);
			case VAR        : return blockVar.getUIActions();
		}
		return "";
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public boolean hasEnded() { return (trialCnt == 0 ? false : trialIdx == trialCnt); }
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public BlockType getType() { return type; }
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public void setBtnOptLbl(int decision, String lbl) {
		if (blockModel == null) return;
		
		blockModel.setBtnOptLbl(decision, lbl);
	}
}
