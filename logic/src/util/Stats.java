package util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;


/**
 * Holds a vector of numerical values and calculates 5-point statistics.
 * 
 * @author Tomek D. Loboda
 */
public class Stats {
	private final List<Double> V = new ArrayList<Double>();  // values
	
	private double min = 0;
	private double max = 0;
	private double mean = 0;
	private double median = 0;
	private double stdev = 0;
	private double sterr = 0;
	
	private boolean isUpdated = false;
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public void addValue(double v) {
		V.add(new Double(v));
		isUpdated = false;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public void addValues(double[] vals) {
		for (int j = 0; j < vals.length; j++) V.add(new Double(vals[j]));
		isUpdated = false;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public void addValues(double[] vals, int step) {
		for (int j = 0; j < vals.length; j += step) V.add(new Double(vals[j]));
		isUpdated = false;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	// Some formulas: http://img135.imageshack.us/img135/9779/calculationslz5.jpg
	public void calculate() {
		if (isUpdated) return;
		
		int N = V.size();
		
		if (N == 0) return;
		
		Collections.sort(V);
		
		min = Double.MAX_VALUE;
		max = Double.MIN_VALUE;
		
		Iterator<Double> it = V.iterator();
		while (it.hasNext()) {
			double v = it.next();
			mean += v;
			if (v < min) min = v;
			if (v > max) max = v;
		}
		mean /= N;
		
		int idxMid = V.size() / 2;
		if (idxMid == 0 || idxMid % 2 == 1) median = V.get(idxMid);
		else median = (V.get(idxMid-1).floatValue() + V.get(idxMid) / 2.0);

		double ss = 0;  // sum of squares
		it = V.iterator();
		while (it.hasNext()) {
			double v = ((Double) it.next()) - mean;
			ss += v*v;
		}
		stdev = (double) Math.sqrt(ss / (N-1));
		
		sterr = stdev / (double) Math.sqrt(N);
		
		isUpdated = true;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Without automatic truncation.
	 */
	public Double getMin    () { calculate(); return (V.size() == 0 ? null : min    ); }
	public Double getMax    () { calculate(); return (V.size() == 0 ? null : max    ); }
	public Double getMean   () { calculate(); return (V.size() == 0 ? null : mean   ); }
	public Double getMedian () { calculate(); return (V.size() == 0 ? null : median ); }
	public Double getStdev  () { calculate(); return (V.size() == 0 ? null : stdev  ); }
	public Double getSterr  () { calculate(); return (V.size() == 0 ? null : sterr  ); }
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * With automatic decimal truncation.
	 */
	public Double getMin    (int ndec) { calculate(); return (V.size() == 0 ? null : $.trunc(min,    ndec)); }
	public Double getMax    (int ndec) { calculate(); return (V.size() == 0 ? null : $.trunc(max,    ndec)); }
	public Double getMean   (int ndec) { calculate(); return (V.size() == 0 ? null : $.trunc(mean,   ndec)); }
	public Double getMedian (int ndec) { calculate(); return (V.size() == 0 ? null : $.trunc(median, ndec)); }
	public Double getStdev  (int ndec) { calculate(); return (V.size() == 0 ? null : $.trunc(stdev,  ndec)); }
	public Double getSterr  (int ndec) { calculate(); return (V.size() == 0 ? null : $.trunc(sterr,  ndec)); }
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Retrieves one-values statistics simulating the regular statistics format.
	 */
	public static String getOneVal(String val) { return val + "\t" + val + "\t" + val + "\t" + val + "\t" + val + "\t" + val; }
	
	
	// ---------------------------------------------------------------------------------
	public int size() { return V.size(); }
}
