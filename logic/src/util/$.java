package util;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;


/**
 * @author Tomek D. Loboda
 */
abstract public class $ {
	public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static String cents2dollars(int cents) {
		double dollars = (double) cents / 100d;
		return "$" + ((dollars == cents / 100) || cents % 10 == 0 ? dollars + "0" : dollars);
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static String date2str(Date d)      { return (d != null ? DATE_FORMAT.format(d) : ""); }
	public static String date2str(Timestamp t) { return (t != null ? DATE_FORMAT.format(t) : ""); }
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static String dbl2currency(double d) {
		String s = "" + d;
		return "" + (d % 10 == 0 ? ((int) d) + ".00" : (s.indexOf(".") == s.length() - 2 ? d + "0" : d));
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static StringBuffer join(String[] A, String prefix, String suffix, String delim, boolean trailDelim, boolean excEmpty) {
		if (A == null) return new StringBuffer();
		
		StringBuffer res = new StringBuffer();
		for (int i = 0; i < A.length; i++) {
			String a = A[i];
			if (excEmpty && (a == null || a.length() == 0)) continue;
			res.append(prefix + a + suffix + delim);
		}
		if (!trailDelim && res.length() > 0) res.delete(res.length() - delim.length(), res.length());
		return res;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static StringBuffer join(List<String> L, String prefix, String suffix, String delim, boolean trailDelim, boolean excEmpty) {
		if (L == null) return new StringBuffer();
		
		StringBuffer res = new StringBuffer();
		for (int i = 0; i < L.size(); i++) {
			String l = L.get(i);
			if (excEmpty && (l == null || l.length() == 0)) continue;
			res.append(prefix + L.get(i) + suffix + delim);
		}
		if (!trailDelim && res.length() > 0) res.delete(res.length() - delim.length(), res.length());
		return res;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static StringBuffer join2(List<Integer> L, String prefix, String suffix, String delim, boolean trailDelim, boolean excEmpty) {
		if (L == null) return new StringBuffer();
		
		StringBuffer res = new StringBuffer();
		for (int i = 0; i < L.size(); i++) {
			String l = "" + L.get(i);
			if (excEmpty && (l == null || l.length() == 0)) continue;
			res.append(prefix + L.get(i) + suffix + delim);
		}
		if (!trailDelim && res.length() > 0) res.delete(res.length() - delim.length(), res.length());
		return res;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static StringBuffer join(Set<String> S, String prefix, String suffix, String delim, boolean trailDelim, boolean excEmpty) {
		if (S == null) return new StringBuffer();
		
		StringBuffer res = new StringBuffer();
		Iterator<String> it = S.iterator();
		while (it.hasNext()) {
			res.append(prefix + it.next() + suffix + delim);
		}
		if (!trailDelim && res.length() > 0) res.delete(res.length() - delim.length(), res.length());
		return res;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static Float str2float(String s, Float min, Float max) {
		Float res = null;
		
		try {
			res = Float.parseFloat(s);
		}
		catch (NumberFormatException ex) {
			return null;
		}
		
		if (min != null && res < min) return null;
		if (max != null && res > max) return null;
		
		return res;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static Integer str2int(String s, Integer min, Integer max) {
		Integer res = null;
		
		try {
			res = Integer.parseInt(s);
		}
		catch (NumberFormatException ex) {
			return null;
		}
		
		if (min != null && res < min) return null;
		if (max != null && res > max) return null;
		
		return res;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static Long str2long(String s, Long min, Long max) {
		Long res = null;
		
		try {
			res = Long.parseLong(s);
		}
		catch (NumberFormatException ex) {
			return null;
		}
		
		if (min != null && res < min) return null;
		if (max != null && res > max) return null;
		
		return res;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static String strRep(String s, int n) {
		return new String(new char[n]).replace("\0", s);
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static double trunc(double d, int ndec) {
		double n = Math.pow(10, ndec);
		return ((double) Math.round(d*n)/n);
	}
}
