package inter;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import main.Do;
import main.Result;


/**
 * @author Tomek D. Loboda
 */
public class DBI {
	public final static int SQL_ERR_DUP_ENTRY = 1062;

	public final static long ID_NONE = 0;

	private Connection conn = null;


	// -----------------------------------------------------------------------------------------------------------------
	public DBI()  throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		conn = Do.datasrc.getConnection();
	}


	// -----------------------------------------------------------------------------------------------------------------
	public void close()  throws SQLException {
		if (conn != null && !conn.isClosed()) conn.close();
	}


	// -----------------------------------------------------------------------------------------------------------------
	public ResultSet execQry(String qry)  throws SQLException {
		return execQry(ResultSet.TYPE_FORWARD_ONLY, qry);
	}


	// -----------------------------------------------------------------------------------------------------------------
	public ResultSet execQry(int type, String qry)  throws SQLException {
		return conn.createStatement(type, ResultSet.CONCUR_READ_ONLY).executeQuery(qry);
	}


	// -----------------------------------------------------------------------------------------------------------------
	public void execUpd(String qry)  throws SQLException {
		Statement st = conn.createStatement();
		st.executeUpdate(qry);
		st.close();
	}


	// -----------------------------------------------------------------------------------------------------------------
	public static String esc(String s) {
		if (s == null) return null;
		return "'" + s.replaceAll("'", "\\\\'") + "'";
	}


	// -----------------------------------------------------------------------------------------------------------------
	public Connection getConn()  throws SQLException { return conn; }


	// ---------------------------------------------------------------------------------
	public static Long getLong(DBI dbi, String qry)  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		Long res = null;

		boolean doCloseConn = false;
		if (dbi == null) {
			dbi = new DBI();
			doCloseConn = true;
		}

		Statement st = dbi.getConn().createStatement();
		ResultSet rs = st.executeQuery(qry);

		if (rs.next() && !rs.wasNull()) res = rs.getLong(1);

		rs.close();
		st.close();
		if (doCloseConn) dbi.close();

		return res;
	}


	// ---------------------------------------------------------------------------------
	public static long getLong(Statement st, String qry)  throws SQLException {
		long res = ID_NONE;
		ResultSet rs = st.executeQuery(qry);
		if (rs.next()) res = rs.getLong(1);
		rs.close();
		return res;
	}


	// ---------------------------------------------------------------------------------
	static public int getRecCnt(ResultSet rs)  throws SQLException {
		rs.last();
		int cnt = rs.getRow();
		rs.beforeFirst();
		return cnt;
	}


	// ---------------------------------------------------------------------------------
	public static String getStr(DBI dbi, String qry)  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		String res = null;

		boolean doCloseConn = false;
		if (dbi == null) {
			dbi = new DBI();
			doCloseConn = true;
		}

		Statement st = dbi.getConn().createStatement();
		ResultSet rs = st.executeQuery(qry);

		if (rs.next()) res = rs.getString(1);

		rs.close();
		st.close();
		if (doCloseConn) dbi.close();

		return res;
	}


	// ---------------------------------------------------------------------------------
	public static String getStr(Statement st, String qry)  throws SQLException {
		String res = null;
		ResultSet rs = st.executeQuery(qry);
		if (rs.next()) res = rs.getString(1);
		rs.close();

		return res;
	}


	// ---------------------------------------------------------------------------------
	/**
	 * Checkes if the 'qrySel' returns an id. If yes, it returns it. Otherwise, it runs
	 * the 'qryIns' and returns the generated id. 'qrySel' should be a SELECT query
	 * returning a single integer ('id') and 'qryIns' should be an INSERT query.
	 */
	public static long insertGetId(Statement stX, String qrySel, String qryIns)  throws SQLException {
		long res = (qrySel == null ? ID_NONE : DBI.getLong(stX, qrySel));

		if (res == ID_NONE) {
			stX.executeUpdate(qryIns, Statement.RETURN_GENERATED_KEYS);
			ResultSet rs = stX.getGeneratedKeys();
			rs.next();
			res = rs.getLong(1);
			rs.close();
		}

		return res;
	}


	// - - - -
	public static long insertGetId(DBI dbi, String qrySel, String qryIns)  throws SQLException {
		Statement st = dbi.newSt();
		long res = insertGetId(st, qrySel, qryIns);
		st.close();
		return res;
	}


	// -----------------------------------------------------------------------------------------------------------------
	public Statement newSt()  throws SQLException { return conn.createStatement(); }


	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * A general routine for setting a value of a given column ('attrName') in a given row ('objId') of a given table
	 * ('objName'). This method handles simple value setting itself and dispatches requests for complex value setting,
	 * e.g. a request to change a variable's type which may require additional operations.
	 *
	 * All data integrity validation should come from the underlying data structure (i.e., the database) constraints
	 * (e.g., unique values, range checks, etc.). Some are checked for below, for performance reasons.
	 */
	public static Result attrSet(String objName, long objId, String attrName, String attrVal)  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		DBI dbiX = new DBI();
		Statement stX = dbiX.transBegin();

		try {
			stX.executeUpdate("UPDATE \"" + objName + "\" SET \"" + attrName + "\" = " + attrVal + " WHERE id = " + objId);
			dbiX.transCommit();

			return new Result(true, "");
		}
		catch (SQLException e) {
			dbiX.transRollback();

			String msg = "";
			if (e.getErrorCode() == SQL_ERR_DUP_ENTRY) {
				msg = "msg:\"Object with that value of the '" + attrName + "' attribute already exists. Values of that attribute must be unique.\"";
			}
			else throw e;

			return new Result(false, msg);
		}
	}


	// -----------------------------------------------------------------------------------------------------------------
	public Statement transBegin()  throws SQLException {
		conn.setAutoCommit(false);
		Statement st = conn.createStatement();
		return st;
	}


	// -----------------------------------------------------------------------------------------------------------------
	public void transCommit()  throws SQLException {
		conn.commit();
		conn.setAutoCommit(true);
		conn.close();
	}


	// -----------------------------------------------------------------------------------------------------------------
	public void transRollback()  throws SQLException {
		conn.rollback();
		conn.setAutoCommit(true);
		conn.close();
	}
}
