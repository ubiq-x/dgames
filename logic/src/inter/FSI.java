package inter;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/**
 * @author Tomek D. Loboda
 */
public class FSI {
	public static final int BUF_SIZE = 8192;
	
	
	// ---------------------------------------------------------------------------------
	/**
	 * Returns the content of the file as a List.
	 */
	public static List<String> file2lst(String filename)  throws IOException {
		List<String> lst = new ArrayList<String>();
		
		BufferedReader in = new BufferedReader(new FileReader(filename));
		String l = null;
		while ((l = in.readLine()) != null) {
			lst.add(l);
		}
		in.close();
		
		return lst;
	}
}
