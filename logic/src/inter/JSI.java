package inter;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;


/**
 * @author Tomek D. Loboda
 */
public class JSI {
	
	// -----------------------------------------------------------------------------------------------------------------
	public static String str2js(String s) {
		//if (s == null) return "\"\"";
		if (s == null) return "null";
		if (s.length() == 0) return "\"\"";
		try {
			s = "$dec(\"" + URLEncoder.encode(s,"UTF-8") + "\")";
		}
		catch (UnsupportedEncodingException e) {}
		return s;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static String str2js(StringBuffer s)  { return str2js(s.toString()); }
	public static String str2js(StringBuilder s) { return str2js(s.toString()); }
}
