package inter;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import main.Result;


/**
 * @author Tomek D. Loboda
 */
public class HTTPI {
	public static final int BUF_SIZE = 8192;
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static String getParamStr(HttpServletRequest req, String name)  throws UnsupportedEncodingException {
		String p = req.getParameter(name);
		try {
			return (p == null ? null : URLDecoder.decode(p, "utf-8").trim());
		}
		catch (IllegalArgumentException e) {
			return (p == null ? null : p.trim());
		}
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static String getParamStr(HttpServletRequest req, String name, String defVal) {
		String param = req.getParameter(name);
		return (param != null ? param : defVal);
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static void retRes(HttpServletResponse res, Result r)  throws IOException {
		HTTPI.retTxt(res, r.getMsg(true));
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static void retHTML(HttpServletResponse res, StringBuffer txt)  throws IOException { retHTML(res, txt.toString()); }
	public static void retHTML(HttpServletResponse res, StringBuilder txt) throws IOException { retHTML(res, txt.toString()); }
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static void retTxt(HttpServletResponse res, StringBuffer txt)  throws IOException { retTxt(res, txt.toString());         }
	public static void retTxt(HttpServletResponse res, StringBuilder txt) throws IOException { retTxt(res, txt.toString());         }
	public static void retTxt(HttpServletResponse res, boolean b)         throws IOException { retTxt(res, (b ? "true" : "false")); }
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static void retStr(HttpServletResponse res, Exception e)  throws IOException {
		res.setContentType("text/plain");
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		e.printStackTrace(pw);
		pw.flush();
		
		PrintWriter out = res.getWriter();
		out.write("{outcome:false,msg:" + JSI.str2js("SERVER SIDE EXCEPTION:\n\n" + sw.getBuffer().toString()) + "}");
		out.close();
		
		pw.close();
		sw.close();
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static void retTxt  (HttpServletResponse res, String cont)  throws IOException { ret(res, "text/plain", BUF_SIZE, cont); }
	public static void retHTML (HttpServletResponse res, String cont)  throws IOException { ret(res, "text/html",  BUF_SIZE, cont); }
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static void ret(HttpServletResponse res, String contType, int buffsize, String cont)  throws IOException {
		res.setContentType(contType);
		res.setBufferSize(buffsize);
		PrintWriter out = res.getWriter();
		out.print(cont);
		out.close();
		res.flushBuffer();
	}
}
