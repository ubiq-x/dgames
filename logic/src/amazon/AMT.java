package amazon;

import inter.DBI;

import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.ParseException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;



/**
 *
 * @author Tomek D. Loboda
 */
@SuppressWarnings("deprecation")
public class AMT {
	//private static final String NTP_SERVER_URL = "time-nw.nist.gov";//"north-america.pool.ntp.org";
		// http://support.ntp.org/bin/view/Servers/WebHome
		// http://tf.nist.gov/tf-cgi/servers.cgi

	private static final String URL_PRODUCTION = "https://mechanicalturk.amazonaws.com";
	private static final String URL_SANDBOX    = "https://mechanicalturk.sandbox.amazonaws.com";

	private static final String ACCESS_KEY = "...";
	private static final String SECRET_KEY = "...";

	private static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
	static { DATE_FORMAT.setTimeZone(TimeZone.getTimeZone("GMT")); }

	//private final RequesterService srv;


	// -----------------------------------------------------------------------------------------------------------------
	public AMT() {
		//srv = new RequesterService(new PropertiesClientConfig("../mturk.properties"));
	}


	// -----------------------------------------------------------------------------------------------------------------
	public static boolean doApproveAssign(String assignId, boolean isProd)  throws KeyManagementException, UnrecoverableKeyException, KeyStoreException, NoSuchAlgorithmException, CertificateException, SignatureException, UnsupportedEncodingException, IOException, ParseException, ParserConfigurationException, SAXException, XPathExpressionException, InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		Document doc = exec("ApproveAssignment", "AssignmentId=" + assignId, isProd);
		boolean isValid = isReqValid(doc, "/");

		if (!isValid) {
			DBI dbi = null;
			try {
				dbi = new DBI();
				int subId      = DBI.getLong(dbi, "SELECT su.id FROM sub su INNER JOIN sess se ON se.sub_id = su.id WHERE se.amt_assign_id = '" + assignId + "'").intValue();
				int amtPayBase = DBI.getLong(dbi, "SELECT e.amt_pay_base FROM exp e INNER JOIN sub su ON su.exp_id = e.id WHERE su.id = " + subId).intValue();
				dbi.execUpd("UPDATE sub SET amt_pay_base = " + amtPayBase + " WHERE id = " + subId);
			}
			finally {
				if (dbi != null) dbi.close();
			}
		}

		return isValid;
	}


	// -----------------------------------------------------------------------------------------------------------------
	public static boolean doCreateHIT(String name, String descr, double reward, String externUrl, int frameH, long assignT, long lifetimeT, int assignCnt, boolean isProd)  throws KeyManagementException, UnrecoverableKeyException, KeyStoreException, NoSuchAlgorithmException, CertificateException, SignatureException, UnsupportedEncodingException, IOException, ParseException, ParserConfigurationException, SAXException, XPathExpressionException {
		Document doc = exec(
			"CreateHIT",
			"Title=" + URLEncoder.encode(name, "UTF-8") + "&" +
			"Description=" + URLEncoder.encode((descr != null && descr.length() > 0 ? descr : "Experiment"), "UTF-8") + "&" +
			"Reward.1.Amount=" + reward + "&" +
			"Reward.1.CurrencyCode=USD&" +
			"Question=" +
			URLEncoder.encode(
				"<ExternalQuestion xmlns=\"http://mechanicalturk.amazonaws.com/AWSMechanicalTurkDataSchemas/2006-07-14/ExternalQuestion.xsd\">" +
				"<ExternalURL>" + externUrl + "</ExternalURL>" +
				"<FrameHeight>" + frameH + "</FrameHeight>" +
				"</ExternalQuestion>",
				"UTF-8"
			) + "&" +
			"AssignmentDurationInSeconds=" + assignT + "&" +
			"LifetimeInSeconds=" + lifetimeT + "&" +
			"MaxAssignments=" + assignCnt + "&" +
			"Keywords=test,%20experiment",
			isProd
		);

		return isReqValid(doc, "/CreateHITResponse/HIT");
	}


	// -----------------------------------------------------------------------------------------------------------------
	public static boolean doRejectAssign(String assignId, boolean isProd)  throws KeyManagementException, UnrecoverableKeyException, KeyStoreException, NoSuchAlgorithmException, CertificateException, SignatureException, UnsupportedEncodingException, IOException, ParseException, ParserConfigurationException, SAXException, XPathExpressionException {
		Document doc = exec("RejectAssignment", "AssignmentId=" + assignId, isProd);
		return isReqValid(doc, "/");
	}


	// -----------------------------------------------------------------------------------------------------------------
	public static Document exec(String operation, String params, boolean isProd)  throws KeyStoreException, NoSuchAlgorithmException, CertificateException, IOException, KeyManagementException, UnrecoverableKeyException, SignatureException, ParserConfigurationException, ParseException, SAXException {
		Document res = null;

		HttpClient hc = null;
		try {
			KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
			trustStore.load(null, null);

			SSLSocketFactory sf = new RelaxedSSLSocketFactory(trustStore);
			sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

			HttpParams hparams = new BasicHttpParams();
			HttpProtocolParams.setVersion(hparams, HttpVersion.HTTP_1_1);
			HttpProtocolParams.setContentCharset(hparams, HTTP.UTF_8);

			SchemeRegistry registry = new SchemeRegistry();
			registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
			registry.register(new Scheme("https", sf, 443));

			ClientConnectionManager ccm = new ThreadSafeClientConnManager(hparams, registry);

			hc = new DefaultHttpClient(ccm, hparams);
			HttpGet hg = new HttpGet(getUrl(operation, params, isProd));
			HttpResponse hres = hc.execute(hg);

			try {
				String xml = EntityUtils.toString(hres.getEntity());
				System.out.println(xml);
				res = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new InputSource(new StringReader(xml)));
			}
			catch (IOException e) {
				e.printStackTrace();
			}
			hg.abort();
		}
		finally {
			if (hc != null) hc.getConnectionManager().shutdown();
		}

		return res;
	}


	// -----------------------------------------------------------------------------------------------------------------
	public static Double getAccountBalance(boolean isProd)  throws KeyManagementException, UnrecoverableKeyException, KeyStoreException, NoSuchAlgorithmException, CertificateException, SignatureException, IOException, XPathExpressionException, ParseException, ParserConfigurationException, SAXException {
		Document doc = exec("GetAccountBalance", "", isProd);
		if (doc == null || !isReqValid(doc, "/GetAccountBalanceResponse/GetAccountBalanceResult")) return null;

		return (Double) XPathFactory.newInstance().newXPath().evaluate("/GetAccountBalanceResponse/GetAccountBalanceResult/AvailableBalance/Amount", doc, XPathConstants.NUMBER);
	}


	// -----------------------------------------------------------------------------------------------------------------
	public static String getUrl(String operation, String params, boolean isProduction)  throws SignatureException, IOException {
		//String time = DATE_FORMAT.format(getTime());
		String time = DATE_FORMAT.format(new Date());
		String sig  = Signature.calculateRFC2104HMAC("AWSMechanicalTurkRequester" + operation + time, SECRET_KEY);
		String url  = (isProduction ? URL_PRODUCTION : URL_SANDBOX) + "/?Service=AWSMechanicalTurkRequester&AWSAccessKeyId=" + ACCESS_KEY + "&Operation=" + operation + "&Signature=" + URLEncoder.encode(sig, "UTF-8") + "&Timestamp=" + URLEncoder.encode(time, "UTF-8") + (params != null && params.length() > 0 ? "&" + params : "") + "";//"&ResponseGroup.0=Minimal&ResponseGroup.1=Request";

		return url;
	}


	// -----------------------------------------------------------------------------------------------------------------
	private static boolean isReqValid(Document doc, String xpath)  throws XPathExpressionException {
		return ((String) XPathFactory.newInstance().newXPath().evaluate(xpath + "/Request/IsValid", doc, XPathConstants.STRING)).equalsIgnoreCase("true");
	}


	// -----------------------------------------------------------------------------------------------------------------
	/*
	public static final Date getTime()  throws IOException {
		Date res = new Date();

		TimeTCPClient client = new TimeTCPClient();
		try {
			client.setDefaultTimeout(10000);
			client.connect(NTP_SERVER_URL);
			res = client.getDate();
		}
		finally {
			client.disconnect();
		}

		return res;
	}
	*/


	// -----------------------------------------------------------------------------------------------------------------
	/*
	public static String exec(String operation)  throws SignatureException, UnsupportedEncodingException {
		StringBuilder res = new StringBuilder();

		URL url;
		try {
			url = new URL(getUrl(operation));
			HttpsURLConnection urlConn = (HttpsURLConnection) url.openConnection();
			BufferedReader in = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));
			String inputLine;
			while ((inputLine = in.readLine()) != null) res.append(inputLine);
			in.close();
		}
		catch (MalformedURLException e) {
			e.printStackTrace();
		}
		catch (IOException e) {
			e.printStackTrace();
		}

		return res.toString();
	}
	*/


	// -----------------------------------------------------------------------------------------------------------------
	/*
	public static String exec(String operation)  throws KeyStoreException, KeyManagementException, UnrecoverableKeyException, NoSuchAlgorithmException, SignatureException, ClientProtocolException, IOException, CertificateException {
		DefaultHttpClient httpclient = new DefaultHttpClient();
		try {
			//KeyStore trustStore  = KeyStore.getInstance(KeyStore.getDefaultType());
			//FileInputStream instream = new FileInputStream(new File("my.keystore"));
			//try {
			//	trustStore.load(instream, "nopassword".toCharArray());
			//}
			//finally {
			//	try { instream.close(); } catch (Exception ignore) {}
			//}
			trustStore.load(null, null);

			//SSLSocketFactory sf = new SSLSocketFactory(trustStore);
			SSLSocketFactory sf = new RelaxedSSLSocketFactory(trustStore);
			sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

			Scheme sch = new Scheme("https", 443, sf);
			httpclient.getConnectionManager().getSchemeRegistry().register(sch);

			HttpGet httpget = new HttpGet(getUrl(operation));

			HttpResponse response = httpclient.execute(httpget);
			HttpEntity entity = response.getEntity();

			System.out.println(response.getStatusLine());
			if (entity != null) {
				System.out.println("Response content length: " + entity.getContentLength());
			}
			EntityUtils.consume(entity);
		}
		finally {
			httpclient.getConnectionManager().shutdown();
		}

		return "...";
	}
	*/


	// -----------------------------------------------------------------------------------------------------------------
	/*
	public Result publish() {
		double balance = srv.getAccountBalance();

		try {
			HIT hit = srv.createHIT("Title", "Description", 0.01, RequesterService.getBasicFreeTextQuestion("What's up?"), 3);
			System.out.println(srv.getWebsiteURL() + "/mturk/preview?groupId=" + hit.getHITTypeId());

			return new Result(true, "account:{balance:{before:" + balance + ",after:" + (balance - reward) + "}},hit:{id:" + hit.getHITId() + "}");
		}
		catch (ServiceException e) {
			return new Result(false, "msg:\"" + JSI.str2js(e.getLocalizedMessage()) + "\"");
		}
	}
	*/
}
