package amazon;

import java.security.SignatureException;

import javax.crypto.Mac;

import javax.crypto.spec.SecretKeySpec;


/**
 * This class defines common routines for generating 
 * authentication signatures for AWS Platform requests.
 */
public class Signature {
	private static final String HMAC_SHA1_ALGORITHM = "HmacSHA1";
	
	
	/**
	 * Computes RFC 2104-compliant HMAC signature.
	 */
	public static String calculateRFC2104HMAC(String data, String key)  throws java.security.SignatureException {
		String result;
		
		try {
			SecretKeySpec signingKey = new SecretKeySpec(key.getBytes(), HMAC_SHA1_ALGORITHM);
			
			Mac mac = Mac.getInstance(HMAC_SHA1_ALGORITHM);
			mac.init(signingKey);
			
			byte[] rawHmac = mac.doFinal(data.getBytes());
			
			result = Encoding.EncodeBase64(rawHmac);
		} 
		catch (Exception e) {
			throw new SignatureException("Failed to generate HMAC : " + e.getMessage());
		}
		
		return result;
	}
}
