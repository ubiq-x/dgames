package amazon;


/**
 * This class defines common routines for encoding data in AWS Platform requests.
 */
public class Encoding {
	
	// -----------------------------------------------------------------------------------------------------------------
    public static String EncodeBase64(byte[] rawData) {
        return Base64.encodeBytes(rawData);
    }
}
