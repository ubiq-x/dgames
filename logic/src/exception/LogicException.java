package exception;


/**
 * @author Tomek D. Loboda
 */
@SuppressWarnings("serial")
public class LogicException extends Exception {
	public final String msg;
	
	
	// ---------------------------------------------------------------------------------
	/**
	 * C'tor.
	 */
	public LogicException(String msg) {
		this.msg = msg;
	}
	
	
	// ---------------------------------------------------------------------------------
	public String getMessage() {
		return msg;
	}
}
